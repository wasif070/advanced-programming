{-# LANGUAGE GADTs #-} 
--Define three different values of type A for the following data type.
{--
data A where
    A :: A
    B :: A -> [A] -> A
 deriving Show

value1 = A
value2 = B value1 [value1]
value3 = B value1 [value2]
--}

--Define three different values of type A Int 
--for the following data type A a that result in Just 73 when passed to sumA
{--
data A a where
    A :: a -> A a -> A a
    B :: a -> A a
    deriving Show
  
sumA :: A Int -> Maybe Int
sumA (A x xs) = case sumA xs of
                    Nothing -> Just x
                    Just s  -> Just (x + s)
sumA (B _)    = Nothing

value1 = A 73 (B 10)
value2 = A 3 (A 70(B 10))
value3 = A 70 (A 3(B 10))
--}

data A a where
    A :: Int -> A a
    B :: A a -> a -> A a
    C :: A (A a) -> Int -> A a
 deriving Show

value1 = A 10
value2 = B (A 10) "Haskell"
value3 = C (A 73) 15