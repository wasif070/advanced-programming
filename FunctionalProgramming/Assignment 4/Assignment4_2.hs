{-# LANGUAGE GADTs #-} 

f :: Int -> Int -> Int
--f x y = x * y + 42
f = \x y -> x * y + 42

g :: Int -> Int
--g x = 42
g = \x -> 42

h :: Bool -> Int -> Bool
--h b x = b && x > 42
h = \b x -> b && x > 42

mapF :: [Int] -> [Int]
mapF xs = map (f 2) xs

mapF' :: [Int] -> [Int]
mapF' xs = map undefined xs

mapG :: [Int] -> [Int]
mapG xs = map g xs

mapH :: Bool -> [Int] -> [Bool]
mapH b xs = map (h b) xs

mapH' :: Int -> [Bool] -> [Bool]
mapH' x bs = map undefined bs

mapH'' :: [Int] -> [Bool] -> [Bool]
mapH'' xs bs = undefined