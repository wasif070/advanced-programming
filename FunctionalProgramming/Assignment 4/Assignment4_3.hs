{-# LANGUAGE GADTs #-} 

type Set a = a -> Bool

setToList :: Set Int -> [Int]
setToList s = [x | x <- [0..], contains s x]

listToSet :: [Int] -> Set Int
listToSet xs = \y -> elem' y xs 

contains :: Set Int -> Int -> Bool
contains s n = s n

elem' :: Int -> [Int] -> Bool
elem' _ []                 = False
elem' x (y:ys) | x == y    = True
               | otherwise = elem' x ys

lostSet :: Set Int
lostSet = \x -> elem' x [4, 8, 15, 16, 23, 42]

--empty :: Set a represents the empty set.
empty' :: Set Int
empty' = \x -> elem' x []

--insert :: Int -> Set Int -> Set Int adds an element to a set.

insert :: Int -> Set Int -> Set Int
insert x s | contains s x = s 
           | otherwise = listToSet (x : setToList s)
            --otherwise = \y -> elem' y (x: setToList s)
           
remove :: Int -> Set Int -> Set Int
remove x s | contains s x = listToSet (filter (/=x) (setToList s))
                            -- \y -> elem' y (filter (/=x) (setToList s))
           | otherwise    = s 

isElem :: a -> Set a -> Bool
isElem n s = s n

union :: Set Int -> Set Int -> Set Int
union s1 s2 = \y -> elem' y ((setToList s1) ++ (setToList s2))

intersection :: Set Int -> Set Int -> Set Int
intersection s1 s2 = \y -> elem' y (getCommonList (setToList s1) s2)
                    where 
                        getCommonList :: [Int] -> Set Int -> [Int]
                        getCommonList [] _     = []
                        getCommonList (x:xs) s | contains s x = x : getCommonList xs s
                                               | otherwise    = getCommonList xs s
                          
difference :: Set Int -> Set Int -> Set Int
difference s1 s2 = \y -> elem' y (getDifferentList (setToList s1) s2)
                        where 
                            getDifferentList :: [Int] -> Set Int -> [Int]
                            getDifferentList [] _     = []
                            getDifferentList (x:xs) s | not (contains s x) = x : getDifferentList xs s 
                                                      | otherwise          = getDifferentList xs s  

universalSet :: Set Int
universalSet = \x -> elem x [0..100]

complement :: Set Int -> Set Int
complement s = difference universalSet s