{-# LANGUAGE GADTs #-} 

incList :: [Int] -> [Int]
incList []     = []
incList (x:xs) = (1+x) : incList xs

negateList :: [Bool] -> [Bool]
negateList []     = []
negateList (x:xs) = not x : negateList xs 

doubleList :: [Int] -> [Int]
doubleList [] = []
doubleList (x:xs) = x*x : doubleList xs

expotList :: [Int] -> [Int]
expotList []     = []
expotList (x:xs) = 2^x : expotList xs 

binaryPotList :: [Int] -> [Int]
binaryPotList []     = []
binaryPotList (x:xs) = x^2 : binaryPotList xs

isZeroList :: [Int] -> [Bool]
isZeroList [] = []
isZeroList (x:xs) = isZero x : isZeroList xs
    where isZero :: Int -> Bool
          isZero 0 = True
          isZero _ = False

hoFunction :: (a -> b) -> a -> b
hoFunction f x = f x

mapList :: (a -> b) -> [a] -> [b]
mapList f []     = []
mapList f (x:xs) = f x : mapList f xs 

incList' :: [Int] -> [Int]
incList' xs = mapList inc xs 
    where
        inc :: Int -> Int
        inc x = 1 + x

incList'' :: [Int] -> [Int]
incList'' xs = mapList (\x -> x +1) xs

negateList' :: [Bool] -> [Bool]
negateList' xs = mapList negate xs
        where
            negate :: Bool -> Bool
            negate x = not x

negateList'' :: [Bool] -> [Bool]
negateList'' xs = mapList (\x -> not x) xs

fortyTwoList :: [a] -> [Int]
fortyTwoList xs = mapList (\x -> const 42 x) xs

foldList :: (a -> b -> b) -> b -> [a] -> b 
foldList f e [] = e
foldList f e (x:xs) = f x (foldList f e xs)

sumOfInts :: [Int] -> Int
sumOfInts xs = foldList (\x curry -> x + curry) 0 xs

sumOfBoolsOr :: [Bool] -> Bool
sumOfBoolsOr xs = foldList(\x curry -> x || curry) False xs

lengthList :: [Int] -> Int
lengthList xs = foldList(\x curry -> 1 + curry)    0 xs

foldListLeft :: (b -> a -> b)  b -> [a] -> b
foldListLeft f e [] = e 
foldListLeft f e (x:xs) = foldListLeft f (f e x) xs

foldl (\x y -> x - 2 * y) (-3) [2,3,4]
-- (((-3)-2*2)-2*3)-2*4
foldr (\x y -> x - 2 * y) (-3) [2,3,4]
--           (2-(3-(4-(2*(-3)))))