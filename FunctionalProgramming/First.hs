{-# LANGUAGE GADTs #-} 
-- constant or nullary function
fortytwo = 42

-- two-ary function (a function with parameters/arguments)
plus n m = n + m

minus n m = n - m

calc = plus 42 (plus 1 2)

someComplicatedCalculation1 n = 
    plus (plus (plus n (plus n 0)) fortytwo)
        (plus (plus n (plus n 0)) fortytwo)

someComplicatedCalculation2 n = plus exp exp
                                    where exp = (plus (plus n (plus n 0)) fortytwo)

someComplicatedCalculation3 n = 
    let exp = (plus (plus n (plus n 0)) fortytwo) in
        plus exp exp

someComplicatedCalculation4 n =
    let plusN0 = plus n 0 in
    let plusNRes = plus n plusN0 in
    let exp = plus plusNRes fortytwo in
    plus exp exp

someComplicatedCalculation5 n = 
    let plusN0 = plus n 0
        plusNRes = plus n plusN0
        exp = plus plusNRes fortytwo in
    plus exp exp

plusCorrect :: Int
plusCorrect = 42 + 1

timesTwo :: Int -> Int
timesTwo n = plus n n

timesTwoPlusM :: Int -> Int -> Int
timesTwoPlusM n m = plus (plus n n) m

floatingOperation :: Float -> Float
floatingOperation fl = fl + 1.0 

(.+.) :: Float -> Float -> Float
(.+.) fl1 fl2 = fl1 + fl2

isZero :: Int -> Bool
isZero 0 = True
isZero n = False

fstPair :: (Int, Int) -> Int
fstPair (n, m) = n

sndPair :: (Int, Int) -> Int
sndPair (n,m) = m

isEven :: Int -> Bool
isEven n =  isZero(sndPair (divMod n 2))

isEven' :: Int -> Bool
isEven' n = isZero (mod n 2)

isOdd :: Int -> Bool
isOdd n = not (isEven n)

fac :: Int -> Int
fac 0 = 1
fac n = n `mult` fac(n -1)

mult n m = n * m

fac' :: Int -> Int
fac' n | n <= 0 = 1
fac' n = n `mult` fac'(n-1)

data OurBool where 
    OurTrue  :: OurBool
    OurFalse :: OurBool
 deriving Show

data IntList where
    NoInt      :: IntList
    OneMoreInt :: Int -> IntList -> IntList
 deriving Show 

intList1 :: IntList
intList1 = NoInt

intList2 :: IntList
intList2 = OneMoreInt 5 intList1

intList3 :: IntList
intList3 = OneMoreInt 42 intList2

hasNoInt :: IntList -> Bool
hasNoInt NoInt = True
hasNoInt l     = False

hasNoInt' :: IntList -> OurBool
hasNoInt' NoInt = OurTrue
hasNoInt' l     = OurFalse

combineTwoIntLists :: IntList -> IntList -> IntList
combineTwoIntLists NoInt NoInt = NoInt
combineTwoIntLists NoInt l1    = l1