--module Fifth where
--    import SimplePrelude

{-# LANGUAGE GADTs #-} 

type Point = (Float, Float)

point1 :: Point
point1 = (3.5, 1.5)

point2 :: Point
point2 = (1.5, 2.0)

point3 :: Point
point3 = (0.0, 0.0)

type Region = Point -> Bool

data RegionAlt = RegionAlt [Point]

empty :: Region
--Point->Bool
empty p = False

whole :: Region
whole p = not (empty p)

--hit :: (Point -> Bool) -> Point -> Bool
hit :: Region -> Point -> Bool
hit r p = r p

--square :: Float -> Region
--square side = rectangle side side

square' :: Float -> Region
square' side = \ p ->
              case p of
                (xCoord, yCoord) -> xCoord <= side && yCoord <= side
