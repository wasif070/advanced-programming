{-# LANGUAGE GADTs #-} 
{--
*Main> play
Let's play rock, paper, scissors!
Rock o, paper [], scissors 8<?
Rock
Invalid move!
Rock o, paper [], scissors 8<?
123
Invalid move!
Rock o, paper [], scissors 8<?
[]
[] VS []
Draw! Play again!
Let's play rock, paper, scissors!
Rock o, paper [], scissors 8<?
[]
[] VS 8<
You lose!
The lines Rock, 123, [] and again [] were entered by the user.
• As a first step, define a helper function getMove :: IO Move that asks the user for input until the
user types one of the move symbols (i.e., o, [], or 8<). For other symbols, the user is notified about
the invalid move. The data type Move is defined below.
• Next, define a function against :: Move -> Move -> Ordering that compares your move with a
computer move.
• Finally, define the function play by means of your helper function getMove to implement a general
framework for the game as illustrated above. The decision about winning and losing is determined
by the result of against. Only a draw results in a rematch, otherwise the game ends. Use rndMove
:: IO Move (without defining it) to choose a random move for the computer player.
--}

data Move where
    Rock     :: Move
    Paper    :: Move
    Scissors :: Move
    deriving Show

getMove :: IO Move
getMove = do 
    putStrLn ("Rock o, Paper [], Scissors 8<?")
    l <- getLine
    case l of 
        "o" -> return Rock
        "[]"-> return Paper
        "8<"-> return Scissors
        _  -> putStrLn ("Invalid Move!") >> getMove

against :: Move -> Move -> Ordering
against Rock     Paper    = LT 
against Rock     Scissors = GT 
against Paper    Rock     = GT
against Paper    Scissors = LT
against Scissors Rock     = LT
against Scissors Paper    = GT
against _        _        = EQ

rndMove :: IO Move
rndMove = undefined

play :: IO ()
play = do 
    putStrLn ("Let's play rock, paper, scissors!") -- String -> IO()
    play'
        where play' = do
                 computerMove <- rndMove
                 player       <- getMove
                 putStrLn (show (player) ++ " VS " ++ show (computerMove))
                 case against plaer computerMove of
                            "LT" -> putStrLn ("You Lose!") >> retrun ()
                            "GT" -> putStrLn ("You won!") >> reuturn ()
                            "EQ" -> putStrLn ("Draw! Play again!") >> play'

