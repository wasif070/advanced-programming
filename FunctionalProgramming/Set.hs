{-# LANGUAGE GADTs #-} 
{--
data Set a where 
    Set :: (a -> Bool) -> Set a
    --deriving Show

empty :: Set a
empty = Set (const False)

insert :: (Eq a) => a -> Set a -> Set a
insert x (Set s) = Set (\y -> x == y || s y)

isElem :: Eq a => a -> Set a -> Bool
isElem x (Set s) = s x

union :: Eq a => Set a -> Set a -> Set a
union (Set s1) (Set s2) = Set (\x -> s1 x || s2 x)

--}

data Set a where
    Set :: [a] -> Set a
    deriving Show

empty :: Set a
empty = Set []

insert :: (Eq a) => a -> Set a -> Set a
insert x (Set s) = Set (if x `elem` s then s else x:s) 

isElem :: (Eq a) => a -> Set a -> Bool
isElem x (Set s) = x `elem` s

union :: (Eq a) => Set a -> Set a -> Set a
union (Set []) s2     = s2
union (Set (x:xs)) s2 = insert x (union (Set xs) s2)

--insert in order
insert' :: (Eq a, Ord a) => a -> Set a -> Set a
insert' x (Set s) = Set (oinsert s)
                        where
                            --oinsert :: a -> List a -> List a
                            oinsert []                 = [x]
                            oinsert (y:ys) | x == y    = y:ys
                                           | x < y     = x:y:ys
                                           | otherwise = y : oinsert ys

isElem' :: (Eq a, Ord a) => a -> Set a -> Bool
isElem' x (Set s) = oisElem s
                        where
                        oisElem []     = False
                        oisElem (y:ys) | x==y      = True
                                       | x<y       = False
                                       | otherwise = oisElem ys
                                

union' :: (Eq a, Ord a) => Set a -> Set a -> Set a
union' (Set s1) (Set s2) = Set (ounion s1 s2)
                            where ounion [] ys         = ys
                                  ounion xs []         = xs
                                  ounion (x:xs) (y:ys) | x == y    = x : ounion xs ys
                                                       | x < y     = x : ounion xs (y:ys)
                                                       | otherwise = y : ounion (x:xs) ys 