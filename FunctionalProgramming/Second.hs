{-# LANGUAGE GADTs #-} 
data IntList where
    NoInt      :: IntList
    OneMoreInt :: Int -> IntList -> IntList
 deriving Show

aSimpleIntList :: IntList
aSimpleIntList = OneMoreInt 1 (OneMoreInt 2 (NoInt)) 

(<++>) :: IntList -> IntList -> IntList
NoInt           <++> ys = ys
OneMoreInt n ns <++> ys = OneMoreInt n (ns <++> ys)

data BoolList where
    NoBool      :: BoolList
    OneMoreBool :: Bool -> BoolList -> BoolList
 deriving Show

aSimpleBoolList1 :: BoolList
aSimpleBoolList1 = NoBool

aSimpleBoolList2 :: BoolList
aSimpleBoolList2 = OneMoreBool True NoBool

aSimpleBoolList3 :: BoolList
aSimpleBoolList3 = OneMoreBool False (OneMoreBool True NoBool)

data List a where
    NoElem :: List a
    OneMoreElem :: a -> List a -> List a
 deriving Show

aDifferentList1 :: List Int
aDifferentList1 = OneMoreElem 45 (OneMoreElem 67 NoElem)

aDifferentList1':: List Int
aDifferentList1'= OneMoreElem 37 (NoElem)

aDifferentList2 :: List Bool
aDifferentList2 = OneMoreElem False (OneMoreElem True NoElem)

(+++)                  :: List a -> List a -> List a
NoElem +++ ys           = ys
OneMoreElem x xs +++ ys = OneMoreElem x (xs +++ ys)

aDifferentBoolList :: List Bool
aDifferentBoolList = OneMoreElem True (OneMoreElem False NoElem)
{--
data Either a b where
    Left :: a -> Either a b 
    Right:: b -> Either a b 
 deriving Show
 --}
combineBoolIntList :: List (Int, Bool)
combineBoolIntList = OneMoreElem (4,True) (OneMoreElem (6, False) NoElem)

combineBoolIntList' :: List (Either Int Bool)
combineBoolIntList' = OneMoreElem (Left 6) (OneMoreElem (Right True) NoElem)

headElem :: List a -> a
headElem (OneMoreElem x xs) = x

data Partial a where
    Defined :: a -> Partial a
    Undefined :: Partial a
 deriving Show

headPartialElem :: List a -> Partial a
headPartialElem NoElem             = Undefined
headPartialElem (OneMoreElem x xs) = Defined x 

data Suit where
    Diamonds :: Suit
    Clubs    :: Suit
    Spades   :: Suit
    Hearts   :: Suit
 deriving Show

data Rank where
    Numeric :: Int -> Rank
    Jack    :: Rank
    Queen   :: Rank
    King    :: Rank
    Ace     :: Rank
 deriving Show

data Card where
    Card :: Suit -> Rank -> Card
 deriving Show