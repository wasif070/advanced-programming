{-# LANGUAGE GADTs #-} 
{--Implement the function any, which takes a predicate p and a list xs and 
checks whether the predicate holds for any element of the list. Specify two different implementations.

An implementation with recursion.
An implementation with a fold function.
Also specify the most common type signature of any.
--}

anyRecursion :: (a -> Bool) -> [a] -> Bool
anyRecursion p []     = False
anyRecursion p (x:xs) = p x || anyRecursion p xs

--foldr :: (a-> b -> b) -> b -> [a] -> b
anyFold :: (a -> Bool) -> [a] -> Bool
anyFold p xs = foldr(\x y -> p x || y) False xs


isSquare :: Integer -> Bool
isSquare n = anyRecursion (<= (n `div` 2)) [x | x <- [2..(n`div`2)], x*x == n]
