{-# LANGUAGE GADTs #-} 
isPalindrome :: String -> Bool
isPalindrome str = (str == reverse str)

isPalindrome' :: String -> Bool
isPalindrome' str = str == reverse' str

reverse' :: String -> String
reverse' []     = []
reverse' (x:xs) = reverse' xs ++ [x]

allAlphabets :: String
allAlphabets = ['a'..'z']
isPangram :: String -> Bool
isPangram xs = check allAlphabets
                where 
                    check []     = True
                    check (y:ys) = elem y xs && check ys

isPangram' :: String -> Bool
isPangram' s = all (`elem` s) ['a'..'z'] 

areAnagrams :: String -> String -> Bool
--areAnagrams [] ys = True
--areAnagrams (x:xs) ys = elem x ys && areAnagrams xs ys
areAnagrams xs ys = qsort(xs) == qsort(ys)

qsort :: (Ord a) => [a] -> [a]
qsort [] = []
qsort (x:xs) = qsort(smaller xs) ++ [x] ++ qsort(larger xs)
                    where 
                        smaller xs = [y | y <- xs, y<=x]
                        larger xs  = [y | y <- xs, y>x]