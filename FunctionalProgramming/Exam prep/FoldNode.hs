{-# LANGUAGE GADTs #-}
data Tree a where
    Empty :: Tree a
    Node  :: a -> Tree a -> Tree a -> Tree a
    deriving Show

treeVal :: Tree Int
treeVal = Node 5 (Node 8 (Node 7 (Empty) (Empty)) (Node 9 (Empty) (Empty))) (Node 10 (Empty) (Empty))

foldTree :: b -> (a-> b-> b ->b) -> Tree a -> b
foldTree base fBranch Empty = base
foldTree base fBranch (Node x tl tr) = fBranch x (foldTree base fBranch tl) (foldTree base fBranch tr) 

count :: Tree Int -> Int
count Empty          = 0
count (Node x tl tr) = 1 + count tl + count tr 
-- count of val

count' :: Tree Int -> Int
count' tree = foldTree 0 (\x tl tr -> 1 + tl + tr) tree

sum1 :: Tree Int -> Int
sum1 Empty = 0
sum1 (Node x tl tr) = x + sum1 tl + sum1 tr

sum1' :: Tree Int -> Int
sum1' tree = foldTree 0 (\x tl tr -> x + tl +tr) tree

toMakeStringOfVals :: Tree Int -> Tree String
toMakeStringOfVals Empty = Empty
toMakeStringOfVals (Node x tl tr) =  Node (show x) (toMakeStringOfVals tl) (toMakeStringOfVals tr)

toMakeStringOfVals' :: Tree Int -> Tree String
toMakeStringOfVals' tree = foldTree Empty (\x tl tr -> Node (show x) tl tr) tree

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f Empty = Empty
mapTree f (Node x tl tr) = Node (f x) (mapTree f tl) (mapTree f tr)

mapTree' :: (a->b) -> Tree a -> Tree b
mapTree' f tree = foldTree Empty (\x tl tr -> Node (f x) tl tr) tree

toList :: Tree Int -> [Int]
toList Empty = []
toList (Node x tl tr) = x : (toList tl ++ toList tr)

toList' :: Tree Int -> [Int]
toList' tree = foldTree [] (\x tl tr -> x : (tl ++ tr)) tree

toString :: Tree Int -> String
toString Empty = ""
toString (Node x tl tr) = show x ++ toString tl ++ toString tr 

toString' :: Tree Int -> String
toString' tree = foldTree "" (\x tl tr -> show x ++ tl ++ tr) tree