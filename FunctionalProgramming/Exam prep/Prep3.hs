{-# LANGUAGE GADTs #-} 

data BExp where 
    Lit :: Bool -> BExp
    And :: BExp-> BExp-> BExp
    Or ::  BExp-> BExp-> BExp
    Neg :: BExp-> BExp
    deriving (Show)



value :: BExp
value = Neg (Or (And (Lit True) (Lit False)) (Or (Lit True) (Lit False)))

-- ¬ ( (True & False) | (True | False) )
-- ¬ (True & False) & ¬ (True | False)

normalize :: BExp -> BExp
normalize (Lit x) = Lit x
normalize (Neg (Lit x)) = (Neg (Lit x))
normalize (Or exp1 exp2) = Or (normalize exp1) (normalize exp2)
normalize (And exp1 exp2) = And (normalize exp1) (normalize exp2)
normalize (Neg expValue) = solveIt expValue
    where
        solveIt (Neg expr) = normalize (expr)
        solveIt (And expr1 exp2) = normalize (Or (Neg expr1) (Neg exp2))
        solveIt (Or expr1 exp2) = normalize (And (Neg expr1) (Neg exp2))
