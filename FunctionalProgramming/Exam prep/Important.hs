{-# LANGUAGE GADTs #-}
data Peano where
    Z :: Peano
    S :: Peano -> Peano
    deriving (Eq, Show)


class SemiGroup a where
    (<+>) :: a -> a -> a


instance SemiGroup Peano where
    (<+>) Z      ys = ys
    (<+>) (S xs) ys = S ((<+>) xs ys)


data BExp where 
    Lit :: Bool -> BExp
    And :: BExp-> BExp-> BExp
    Or ::  BExp-> BExp-> BExp
    Neg :: BExp-> BExp
    deriving (Show)



value :: BExp
value = Neg (Or (And (Lit True) (Lit False)) (Or (Lit True) (Lit False)))

-- ¬ ( (True & False) | (True | False) )
-- ¬ (True & False) & ¬ (True | False)

normalize :: BExp -> BExp
normalize (Lit x) = Lit x
normalize (Neg (Lit x)) = (Neg (Lit x))
normalize (Or exp1 exp2) = Or (normalize exp1) (normalize exp2)
normalize (And exp1 exp2) = And (normalize exp1) (normalize exp2)
normalize (Neg expValue) = solveIt expValue
    where
        solveIt (Neg expr) = normalize (expr)
        solveIt (And expr1 exp2) = normalize (Or (Neg expr1) (Neg exp2))
        solveIt (Or expr1 exp2) = normalize (And (Neg expr1) (Neg exp2))


    
--i)  Leaf labeled tree
--ii) Node labeled tree 
--iii) Node and leaf labeled tree

data Tree a where
    Leaf :: a -> Tree a
    Node :: Tree a -> Tree a -> Tree a
    deriving (Show)


--ii) data Tree a where
--    Leaf :: Tree a
--    Node :: a -> Tree a -> Tree a -> Tree a
--
--
--iii) data Tree a where
--    Leaf :: a -> Tree a
--    Node :: a -> Tree a -> Tree a -> Tree a 
--

treeVal :: Tree Int 
treeVal = Node (Node (Leaf 4) (Leaf 5)) (Node (Leaf 10) (Leaf 15))

--"451015"
--Node (Node (Leaf "4") (Leaf "5")) (Node (Leaf "10") (Leaf "15"))

--leaf labeled
foldTree :: (a -> b) -> (b -> b -> b) -> (Tree a) -> b
foldTree funcLeaf funcBranch (Leaf x) = funcLeaf x 
foldTree funcLeaf funcBranch (Node lt rt) 
     = funcBranch (foldTree funcLeaf funcBranch lt) (foldTree funcLeaf funcBranch rt)

--node labeled
foldTree :: b -> (a -> b -> b -> b) -> (Tree a) -> b
foldTree base funcBranch Empty = base
foldTree base funcBranch (Node x lt rt) = funcBranch x (foldTree base funcBranch lt) (foldTree base funcBranch rt)

--node and leaf labeled
foldTree :: (a->b) -> (a->b->b->b) -> (Tree a) -> b
foldTree fleaf fbranch (Leaf x) = fleaf x
foldTree fleaf fbranch (Node x tl tr) = fbranch x (foldTree fleaf fbranch tl) (foldTree fleaf fbranch tr)

--leaf labeled
--foldTree (\x -> 1) (\x y -> x + y) treeVal  --> to count
--foldTree (\x -> x) (\x y -> x + y) treeVal  --> to sum
--foldTree (\x -> show x) (\x y -> x ++ y) treeVal --> to make string
--foldTree (\x -> Leaf (show x))  (\x y -> Node x y) treeVal --> to convert in different type
--foldTree (\x -> Leaf (f x)) (\x y -> Node x y) treeVal --> mapTree

--leaf - node labeled
--foldTree (\x -> 1) (\v x y -> x + y) treeVal --> to count
--foldTree (\x -> x) (\v x y -> v + x + y) treeVal --> to sum
--foldTree (\x -> show (x)) (\v x y -> v: (x ++ y)) treeVal --> to make string
--foldTree (\x -> Leaf (show x)) (\v x y -> Node (show v) x y) treeVal --> to Make String
--foldTree (\x -> Leaf (f x)) (\v x y -> Node v x y) treeVal --> mapTree

--Node labeled
--foldTree 1 (\v x y -> x+y)
 
convertThis :: Tree Int -> Tree String
convertThis (Leaf x) = (Leaf (show x))
convertThis (Node lt rt) = Node (convertThis lt) (convertThis rt)


count :: Tree a -> Int 
count (Leaf x) = 1 
count (Node lt rt) = count lt + count rt 

sumTree :: Tree Int -> Int 
sumTree  (Leaf x)  = x
sumTree (Node lt rt) = sumTree lt  + sumTree rt


Maybe (Maybe Bool)
(Maybe Bool)

Just Just True
Just Just False
Just Nothing
Nothing

data A a where
    B :: () -> A a
    A :: (a -> a) -> A a

value1 = B ()
value2 = A id
value2 = A ()





data Tree a where
    Leaf :: Tree a
    Node :: Tree a -> a -> Tree a -> Tree a
    
instance (Eq a) => Eq (Tree a) where 
    Leaf           == Leaf           = True
    (Node l1 x r1) == (Node l2 y r2) = x == y && l1 == l2 && r1 == r2
    _              ==  _             = False

instance (Ord a) => Ord (Tree a) where
    compare (Leaf) (Leaf) = EQ
    compare (Node l1 x r1)(Node l2 y r2) = 
        case compare x y of 
            EQ -> case compare l1 l2 of
                    EQ -> compare r1 r2
                    LT -> LT
                    GT -> GT
            LT -> LT
            GT -> GT

