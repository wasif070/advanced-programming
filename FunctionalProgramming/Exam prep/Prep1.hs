{-# LANGUAGE GADTs #-} 
import Prelude hiding (Monoid)

data Peano where
    Z :: Peano
    S :: Peano -> Peano
    deriving (Eq,Show)

class SemiGroup a where
    (<+>) :: a -> a -> a
    
class SemiGroup a => Monoid a where
    neutral :: a

var1 :: Peano
var1 = S (S (S Z))

var2 :: Peano
var2 = S (S (S (S Z)))

instance SemiGroup Peano where
    (<+>) Z      ys = ys
    (<+>) (S xs) ys = S ((<+>) xs ys)
    
instance Monoid Peano where 
    neutral = Z
    --addP x y = x <+> y 

fun :: (a->[b])-> [a]->[b]
fun f [] = []
fun f (x:xs) = f x ++ fun f xs