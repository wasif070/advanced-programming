{-# LANGUAGE GADTs #-}
data Tree a where
    Leaf :: a -> Tree a
    Node :: a -> Tree a -> Tree a -> Tree a
    deriving Show

treeVal :: Tree Int
treeVal = Node 5 (Node 8 (Node 7 (Leaf 15) (Leaf 13)) (Node 9 (Leaf 11) (Leaf 12))) (Node 10 (Leaf 17) (Leaf 19))

foldTree :: (a->b) -> (a->b->b->b) -> Tree a -> b
foldTree fLeaf fBranch (Leaf x) = fLeaf x
foldTree fLeaf fBranch (Node x tl tr) = fBranch x (foldTree fLeaf fBranch tl) (foldTree fLeaf fBranch tr)

count :: Tree Int -> Int 
count (Leaf x) = 1
count (Node x tl tr) = 1 + count tl + count tr

count' :: Tree Int -> Int
count' tree = foldTree (\x->1)(\x tl tr -> 1 + tl +tr) tree

sum1 :: Tree Int -> Int
sum1 (Leaf x) = x
sum1 (Node x tl tr) = x + sum1 tl + sum1 tr

sum1' :: Tree Int -> Int
sum1' tree = foldTree (\x-> x) (\x tl tr -> x + tl +tr) tree

toStringTree :: Tree Int -> Tree String 
toStringTree (Leaf x) = Leaf (show x)
toStringTree (Node x tl tr) = Node (show x) (toStringTree tl) (toStringTree tr)

toStringTree' :: Tree Int -> Tree String
toStringTree' tree = foldTree (\x -> Leaf (show x)) (\x tl tr -> Node (show x) tl tr) tree

toList :: Tree Int -> [Int]
toList (Leaf x) = [x]
toList (Node x tl tr) = x: (toList tl ++ toList tr)

toList' :: Tree Int -> [Int]
toList' tree = foldTree (\x->[x]) (\x tl tr-> x: (tl ++ tr)) tree

mirror :: Tree Int -> Tree Int
mirror (Leaf x) = Leaf x
mirror (Node x tl tr) = Node x (mirror tr) (mirror tl)

mirror' :: Tree Int -> Tree Int
mirror' tree = foldTree (\x -> Leaf x) (\x tl tr -> Node x tr tl) tree

mapTree :: (a -> b) -> Tree a -> Tree b 
mapTree f (Leaf x) = Leaf (f x)
mapTree f (Node x tl tr) = Node (f x) (mapTree f tl) (mapTree f tr)

mapTree' :: (a->b) -> Tree a -> Tree b 
mapTree' f tree = foldTree (\x -> Leaf (f x)) (\x tl tr -> Node (f x) tl tr) tree

toStringVal :: (Show a) => Tree a -> String
toStringVal (Leaf x) = show x
toStringVal (Node x tl tr) = show x ++ toStringVal tl ++ toStringVal tr

toStringVal' :: (Show a) => Tree a -> String
toStringVal' tree = foldTree (\x -> show x) (\x tl tr -> show x ++ tl ++ tr) tree