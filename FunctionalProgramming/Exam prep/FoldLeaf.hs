{-# LANGUAGE GADTs #-}
--Leaf labeled
data Tree a where
    Leaf :: a -> Tree a
    Node :: Tree a -> Tree a -> Tree a
    deriving Show

foldTree :: (a->b) -> (b->b->b) -> Tree a -> b
foldTree fLeaf fBranch (Leaf x) = fLeaf x 
foldTree fLeaf fBranch (Node tl tr) = fBranch (foldTree fLeaf fBranch tl) (foldTree fLeaf fBranch tr)

treeVal :: Tree Int 
treeVal = Node (Node (Leaf 4) (Leaf 5)) (Node (Leaf 10) (Leaf 15))

count :: Tree Int -> Int
count t = foldTree (\x -> 1) (\x y -> x + y) t

sum' :: Tree Int -> Int
sum' t = foldTree (\x -> x) (\x y -> x+y) t

mirrorTree :: Tree Int -> Tree Int 
mirrorTree (Leaf x) = Leaf x 
mirrorTree (Node tl tr) = Node (mirrorTree tr) (mirrorTree tl)

mirrorTree' :: Tree Int -> Tree Int
mirrorTree' t  = foldTree (\x -> Leaf x) (\x y -> Node y x) t

toList :: Tree a -> [a]
toList (Leaf x) = [x]
toList (Node tl tr) = toList tl ++ toList tr

toList' :: Tree a -> [a]
toList' t = foldTree (\x -> [x]) (\x y -> x ++ y) t

height :: Tree a -> Int
height (Leaf x) = 1
height (Node tl tr) = 1 + max (height tl) (height tr)

height' :: Tree a -> Int
height' t = foldTree (\x -> 1) (\x y -> 1 + max x y) t

treeVal1 :: Tree Int
treeVal1 = Node (Node (Node (Leaf 4) (Leaf 5)) (Leaf 6)) (Node (Node (Leaf 1) (Leaf 2)) (Leaf 3))

leafNumber :: Tree a -> Int
leafNumber (Leaf x) = 1
leafNumber (Node tl tr) = leafNumber tl + leafNumber tr

leafNumber' :: Tree a -> Int
leafNumber' t = foldTree (\x -> 1) (\x y -> x + y) t

unzipTree :: Tree (a,b) -> (Tree a, Tree b)
unzipTree (Leaf (x,y)) = (Leaf x, Leaf y)
unzipTree (Node tl tr) = let (tl1, tr1) = unzipTree tl
                             (tl2, tr2) = unzipTree tr
                             in (Node tl1 tl2, Node tr1 tr2)

--zTree :: Tree (Int, Int)
--zTree = Node' (Node' (Node' (Leaf' (1,0)) (Leaf' (2,1))) (Leaf' (3,2))) (Leaf' (4,3))

unzipTree' :: Tree(a,b) -> (Tree a, Tree b)
unzipTree' t = foldTree (\(x,y) -> (Leaf x, Leaf y)) (\tl tr -> let (tl1, tr1) = tl
                                                                    (tl2, tr2) = tr
                                                                in (Node tl1 tl2, Node tr1 tr2)) t

toString :: Tree Int -> Tree String 
toString (Leaf x) = Leaf (show x)
toString (Node tl tr) = Node (toString tl) (toString tr)

toString' :: Tree Int -> Tree String
toString' tree = foldTree (\x -> Leaf (show x)) (\x y -> Node x y) tree