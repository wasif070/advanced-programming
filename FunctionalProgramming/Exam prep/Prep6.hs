{-# LANGUAGE GADTs #-} 

data A a where
    B :: () -> A a
    A :: (a -> a) -> A a

{--
value1 = B ()
value2 = A id
value2 = A ()

value1 :: A Bool
value1 = B () 

value2 :: A Bool
value2 :: B (id)

value3 :: A Bool
value3 :: B (function1)

function1 :: a->a
function1 x = x
--}
data Exp where
    Num :: Int -> Exp
    Add :: Exp -> Exp -> Exp
    Mul :: Exp -> Exp -> Exp
    --deriving Show
--ghci> show (Mul (Num 42) (Add (Num 1) (Num 1)))
--"(42 * (1 + 1))"

instance Show Exp where
    show (Num a) = show a 
    show (Add exp1 exp2) = "(" ++ (show exp1) ++ "+" ++ (show exp2) ++ ")"
    show (Mul exp1 exp2) = "(" ++ (show exp1)  ++ "*" ++ (show exp2) ++ ")"
    
{--
Which of the following lists will yield a type error?

x
 [[], []]

□
 [(), (1), (2)]

□
 [[1], [False]]

□
 [False, [True]]

x
 [[1, 2], []]
 
 --
 Which of the following types are valid for the expression [(1, True), (0, False)]?

x□
 [Pair]

x□
 [(Int, Bool)]

x□
 Ord a => [(a, Bool)]

□
 [Int, Bool]

□
 [(Int, Bool), (Int, Bool)]

--
Which of the following types are valid for the function fun x y = x y y?

□
 a -> a -> a

□
 (b -> b -> a) -> b -> a

□
 (a -> b -> b) -> b -> a

□
 (a -> a -> a) -> a -> a

□
 (a -> a -> b) -> a -> b
--}

main2 :: IO()
main2 = do
    str <- getLine
    let helper str = str ++ helper str
    if (str == reverse str) then print str else print (helper str)
  