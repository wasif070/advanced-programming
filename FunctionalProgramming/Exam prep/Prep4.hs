{-# LANGUAGE GADTs #-} 

type KVS a b = a -> Maybe b

empty :: KVS a b
empty = (\x -> Nothing)

--insert receives a key, a value and a KVS and inserts the value into the KVS for the passed key.
insert :: (Eq a) => a -> b -> (KVS a b) -> (KVS a b)
insert x y s = \z -> if x == z then Just y else s z

--remove receives a key and a KVS and removes the corresponding entry from the KVS.
remove:: (Eq a) => a -> (KVS a b) -> (KVS a b)
remove x s = \z -> if x == z then Nothing else s z

--find receives a key and a KVS and, if available, returns the value stored for the key in the KVS.
find :: (Eq a) => a -> (KVS a b) -> Maybe b
find x s = s x

--toList receives a KVS and returns all entries of the KVS as a list of key-value pairs.
toList :: (KVS Int String) -> [(Int , Maybe String)]
toList s = toList' [1..100]
             where 
                toList' [] = []
                toList' (y:ys) | find y s == Nothing = toList' ys 
                               | otherwise           = (y, (find y s)) : toList' ys 

--fromList receives a list of key-value pairs and provides a KVS with corresponding entries.
-- If a key occurs more than once, its last occurrence in the list is preferred.
fromList :: (Eq a) => [(a,b)] -> KVS a b
fromList []          = empty
fromList ((x,y):xys) = insert x y (fromList xys) 