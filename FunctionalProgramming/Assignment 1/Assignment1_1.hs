{-# LANGUAGE GADTs #-} 
-- Define a recursive function `exponentiation :: Int -> Int -> Int` that calculates the
-- power of an integer `b` for an integer exponent `e` with `e >= 0` according to the
-- following mathematical definition.
--
--                        / 1                            if e = 0
-- exponentiation(b, e) = \ b * exponentiation(b, e - 1) if e /= 0

exponentiation :: Int -> Int -> Int
exponentiation b e | e == 0    = 1
                   | otherwise = b * (exponentiation b (e -1)) 

-- Implement an optimized recursive function `optExponentiation :: Int -> Int -> Int`
-- that calculates the power of an integer `b` for an integer exponent `e` with `e >= 0`
-- according to the following mathematical definition.
--
-- The optimization in this variant consists of dividing the exponent in each calculation
-- step by two. In particular, make sure that the partial results of the recursive call for
-- an odd exponent are not unnecessarily calculated twice.
--
--                           / 1                                     if e = 0
-- optExponentiation(b, e) = | optExponentiation(b, e / 2) ^ 2       if e /= 0 && e even
--                           \ b * optExponentiationen(b, e / 2) ^ 2 if e /= 0 && e odd

optExponentiation :: Int -> Int -> Int
optExponentiation b e | e == 0                 = 1
                      | e /= 0 && even e       = let exp = optExponentiation b (div e 2)
                                                  in exp * exp 
                      | e /= 0 && not (even e) = let exp = optExponentiation b (div e 2)
                                                  in b * exp * exp

-- Hint: For integer division you can use the predefined function
-- `div :: Int -> Int -> Int`. To determine if a number is even, you can use the predefined
-- function `even :: Int -> Bool`.