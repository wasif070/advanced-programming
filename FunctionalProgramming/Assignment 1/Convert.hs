-- In this exercise we want to convert between binary and decimal number representations.
-- The general scheme for such a conversion is explained at the following website
--   https://www.cs.indiana.edu/cgi-pub/c211/snake/#bin2dec
-- and is known by the name Horner's method.
{-# LANGUAGE GADTs #-} 

    
    data BoolList where
      NoBool      :: BoolList
      OneMoreBool :: Bool -> BoolList -> BoolList
      deriving Show
    
    implementMe = error "You need to implement this function!"
    
    -- Implement a function `decToBinList :: Int -> BoolList` that converts an integer value to a
    -- list of Booleans using Horner's method.
    -- Here, the list value `OneMoreBool True (OneMoreBool False (OneMoreBool False NoBool))`
    -- represents the number 100 in binary (i.e., 4 in the decimal representation).
    
    reverseList :: BoolList -> BoolList
    reverseList l = rev l NoBool
      where
    rev NoBool             ys = ys
    rev (OneMoreBool x xs) ys = rev xs (OneMoreBool x ys)

    decToBinList :: Int -> BoolList
    decToBinList dec = reverseList (decToBinList' dec)
      where
        -- Here, the binary representation is in reversed order, thus, we reverse
        -- it after computing the list.
        decToBinList' :: Int -> BoolList
        decToBinList' 1   = OneMoreBool True NoBool
        decToBinList' dec
          | even dec      = OneMoreBool False (decToBinList' (dec `div` 2))
          | odd dec       = OneMoreBool True (decToBinList' (dec `div` 2))
{-- 
    decToBinList' :: Int -> BoolList
    decToBinList' 1   = OneMoreBool True NoBool
    decToBinList' dec
        | even dec      = OneMoreBool False (decToBinList' (dec `div` 2))
        | odd dec       = OneMoreBool True (decToBinList' ((dec - 1) `div` 2))
--}

    (<++>) :: BoolList -> BoolList -> BoolList
    NoBool           <++> ys = ys
    OneMoreBool x xs <++> ys = OneMoreBool x (xs <++> ys)
    
    -- Instead of reversing the list at the end, we can concatenate the new bit
    -- at the end.
    decToBinList2 :: Int -> BoolList
    decToBinList2 1   = OneMoreBool True NoBool
    decToBinList2 dec
      | even dec      = decToBinList2 (dec `div` 2) <++> OneMoreBool False NoBool
      | odd dec       = decToBinList2 (dec `div` 2) <++> OneMoreBool True NoBool