{-# LANGUAGE GADTs #-} 


data BoolList where
  NoBool      :: BoolList
  OneMoreBool :: Bool -> BoolList -> BoolList
  deriving Show

{--
reverseList :: [Int] -> [Int]
reverseList []     = []
reverseList (x:xs) = reverseList xs ++ [x]
--}

reverseList :: BoolList -> BoolList
reverseList l = rev l NoBool
                 where rev NoBool ys             = ys
                       rev (OneMoreBool x xs) ys = rev xs (OneMoreBool x ys)

decToBinList :: Int -> BoolList
decToBinList dec = reverseList (decToBinList' dec)
                    where
                      decToBinList' :: Int -> BoolList
                      decToBinList' 1   = OneMoreBool True NoBool
                      decToBinList' dec | even dec = OneMoreBool False (decToBinList' (dec `div` 2))
                                        | odd dec  = OneMoreBool True (decToBinList' (dec `div` 2))

(<++>) :: BoolList -> BoolList -> BoolList
NoBool <++> ys           = ys
OneMoreBool x xs <++> ys = OneMoreBool x (xs <++> ys)


decToBinList'' :: Int -> BoolList
decToBinList'' 1   = OneMoreBool True NoBool
decToBinList'' dec | even dec = decToBinList'' (dec `div` 2) <++> OneMoreBool False NoBool
                   | odd dec  = decToBinList'' (dec `div` 2) <++> OneMoreBool True NoBool

                  

reverseList' :: BoolList -> BoolList -> BoolList
reverseList' NoBool ys             = ys
reverseList' (OneMoreBool x xs) ys = reverseList' xs (OneMoreBool x ys)  

binListToDec :: BoolList -> Int
binListToDec OneMoreBool False NoBool = error "Not a binary"
binListToDec NoBool                   = error "Not a binar"
binListToDec xs = binListToDec' (reverseList' xs)



binListToDec' OneMoreBool True NoBool = 1
binListToDec' OneMoreBool b bs | b == True = 2 * binListToDec' bs + 1
                               | otherwise = 2 * binListToDec' bs 