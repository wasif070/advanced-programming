{-# LANGUAGE GADTs #-} 

fib :: Int -> Int
fib n | n == 0    = 0
      | n == 1    = 1
      | otherwise = fib(n-1) + fib(n-2)

--fibonacci using accumulator
fib' :: Int -> Int
fib' n = fib'' 0 1 n
            where fib'' :: Int -> Int -> Int -> Int
                  fib'' fibn fibnp1 n | n == 0    = fibn
                                      | otherwise = fib'' fibnp1 (fibn + fibnp1) (n-1) 
data IntList where
    NoInt      :: IntList
    OneMoreInt :: Int -> IntList -> IntList
 deriving Show

data IntTree where
    Leaf :: Int -> IntTree
    Node :: IntTree -> IntTree -> IntTree
 deriving Show

testTree :: IntTree
testTree = Node (Node (Leaf 1) (Leaf 2)) (Leaf 3)

sumTree :: IntTree -> Int
sumTree (Leaf n)                  = n 
sumTree (Node leftTree rightTree) = sumTree leftTree + sumTree rightTree

--Using accumulator like fib

sumTree2 :: IntTree -> Int
sumTree2 (Leaf x) = x
sumTree2 (Node leftTree rightTree) = sumTree2' (Node leftTree rightTree) 0

sumTree2' :: IntTree -> Int -> Int
sumTree2' (Leaf x) sum = sum + x
sumTree2' (Node leftTree rightTree) sum = sum + (sumTree2' leftTree sum) + (sumTree2' rightTree sum)

mirrorTree :: IntTree -> IntTree
mirrorTree (Leaf x) = Leaf x
mirrorTree (Node leftTree rightTree) = Node (mirrorTree rightTree) (mirrorTree leftTree) 

(<++>) :: IntList -> IntList -> IntList
NoInt <++> ys           = ys
OneMoreInt x xs <++> ys = OneMoreInt x (xs <++> ys)

toList :: IntTree -> IntList
toList (Leaf x)                  = OneMoreInt x NoInt
toList (Node leftTree rightTree) = toList leftTree <++> toList rightTree

toList' :: IntTree -> [Int]
toList' (Leaf x)     = [x]
toList' (Node tl tr) = toList' tl ++ toList' tr

data Tree a where
    Leaf' :: a -> Tree a
    Node' :: Tree a -> Tree a -> Tree a
    deriving Show

tree1 :: Tree Int 
tree1 = Node' (Node' (Node' (Leaf' 1) (Leaf' 2)) (Leaf' 3)) (Leaf' 4)

--tree2 :: Tree (Tree Int)
--tree2 = Node tree1 tree1

flatTree :: Tree (Tree Int) -> Tree Int 
flatTree (Leaf' t) = t 
flatTree (Node' tl tr) = Node' (flatTree tl) (flatTree tr)

leabelTree :: Tree a -> Tree (a , Int)
leabelTree t = fst (leabelTree' t 0)
            where leabelTree' :: Tree a -> Int -> (Tree (a, Int), Int)
                  leabelTree' (Leaf' x) n    = (Leaf' (x, n), n+1)
                  leabelTree' (Node' tl tr) n = let (tl', n1) = leabelTree' tl n
                                                    (tr', n2) = leabelTree' tr n1 
                                                   in (Node' tl' tr', n2)   

zTree :: Tree (Int, Int)
zTree = Node' (Node' (Node' (Leaf' (1,0)) (Leaf' (2,1))) (Leaf' (3,2))) (Leaf' (4,3))

unzipTree :: Tree (a,b) -> (Tree a, Tree b)
unzipTree (Leaf' (x,y)) = (Leaf' x, Leaf' y)
unzipTree (Node' tl tr) = let (tl1, tr1)  = unzipTree tl
                              (tl2, tr2)  = unzipTree tr 
                        in (Node' tl1 tl2, Node' tr1 tr2)

uzTree :: (Tree Int, Tree Int)
uzTree = (Node' (Node' (Node' (Leaf' 1) (Leaf' 2)) (Leaf' 3)) (Leaf' 4),
            Node' (Node' (Node' (Leaf' 0) (Leaf' 1)) (Leaf' 2)) (Leaf' 3))

zipTree :: Tree a -> Tree b -> Tree (a,b)
zipTree (Leaf' x) (Leaf' y)             = Leaf' (x,y)
zipTree (Node' tl1 tr1) (Node' tl2 tr2) = Node' (zipTree tl1 tl2) (zipTree tr1 tr2)
--zipTree (Node _ _) (Leaf _)           = undefined
--zipTree (Leaf _) (Node _ _)           = undefined
--The problem is in the last two conditions, we dont have a value which might represent Nothing.
--We have to define Node labeled trees