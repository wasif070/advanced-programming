{-# LANGUAGE GADTs #-} 

data List a where
    NoElem      :: List a
    OneMoreElem :: a -> List a -> List a
    deriving Show

--count 4 (OneMoreElem 1 (OneMoreElem 2 (OneMoreElem 4 NoElem)))
--1                          
count :: (Num a, Eq a) => a -> List a -> a
count _ NoElem                         = 0
count e (OneMoreElem x xs) | e == x    = 1 + count e xs
                           | otherwise = count e xs

--count' 3 [1,3,5,2,3]
--2
count' :: Int -> [Int] -> Int
count' _ [] = 0
count' e (x:xs) | e == x = 1 + count' e xs
                | otherwise = count' e xs

--remove 3 [1,2,3,4]
--[1,2,4]
remove :: Int -> [Int] -> [Int]
remove _ []                 = []
remove e (x:xs) | e /= x    = x: remove e xs 
                | otherwise = remove e xs

---remove' 3 (OneMoreElem 1 (OneMoreElem 2 (OneMoreElem 3 NoElem))) 
--OneMoreElem 1 (OneMoreElem 2 NoElem)               
remove' :: (Num a, Eq a) => a -> List a -> List a
remove' _ NoElem                         = NoElem
remove' e (OneMoreElem x xs) | e == x    = remove' e xs 
                             | otherwise = OneMoreElem x (remove' e xs)

--sublist 2 5 [0..10]
--[2,3,4]
sublist :: Int -> Int -> [Int] -> [Int]
sublist a b (x:xs) | b < 0     = [] 
                   | a > 0     = sublist (a-1) (b-1) xs
                   | otherwise = [x] ++ sublist 0 (b-1) xs

--
sublist' :: (Num a, Eq a) => a -> a -> List a -> List a
sublist' _ _ NoElem            = NoElem
sublist' _ 0 (OneMoreElem x _) = OneMoreElem x NoElem
sublist' 0 m (OneMoreElem x xs)= OneMoreElem x (sublist' 0 (m-1) xs)
sublist' n m (OneMoreElem x xs)= sublist' (n-1) (m-1) xs
 
--[1,2,3,4]
--[4,3,2,1]
reversed :: [Int] -> [Int]
reversed []     = []
reversed [x]    = [x]
reversed (x:xs) = reversed xs ++ [x]  

{--
(+++) :: List a -> List a -> List a
NoElem +++ ys           = ys
OneMoreElem x xs +++ ys = OneMoreElem x (xs +++ ys)

reversed' :: List a -> List a
reversed' NoElem           = NoElem
reversed' OneMoreElem x xs = reversed' xs +++ OneMoreElem x NoElem
---}
{-
reversed [5,9,2,4] => reversed [9,2,4] ++ [5]
				   => reversed [2, 4] ++ [9] ++ [5]
				   => reversed [4] ++ [2] ++ [9] ++ [5]
				   => reversed [] ++ [4] ++ [2] ++ [9] ++ [5]
				   => [] ++ [4] ++ [2] ++ [9] ++ [5]
It seems like the complexity for this solution is =  O(n), 
But we have to consider the complexity of (++) operation which is = O(n). 
So the overall complexity of this solution is O(n*n) 
That's why it works slow in bigger inputs. 
-}
--
reversed2 :: [Int] -> [Int]
reversed2 xs = reversed2' xs []
               where reversed2' :: [Int] -> [Int] -> [Int]
                     reversed2' [] a     = a
                     reversed2' (x:xs) a = reversed2' xs (x:a) 
{-
The complexity for this solution is =  O(n), because complexity of (:) is O(1) 
That's why it works faster in bigger inputs. 
-}

reversedAcc :: List a -> List a
reversedAcc l = rev l NoElem
                where rev NoElem ys             = ys
                      rev (OneMoreElem x xs) ys = rev xs (OneMoreElem x ys)  

--
indexOf :: Int -> [Int] -> Int
indexOf n []     = -1
indexOf n (x:xs) = indexOf' n (x:xs) 0
                    where indexOf' :: Int -> [Int] -> Int -> Int
                          indexOf' n (x:xs) carry | n == x = carry
                                                  | otherwise = indexOf' n xs (carry + 1)

--
data Partial a where
    Defined   :: a -> Partial a 
    Undefined :: Partial a 
 deriving Show

indexOf1 :: (Num a) => a -> List a -> Partial a
indexOf1 = index 0
            where
                index :: (Num a) => a -> a -> List a -> Partial a 
                index _ _ NoElem                         = Undefined
                index i x (OneMoreElem y ys) | x == y    = i 
                                             | otherwise = index (i+1) OneMoreElem ys
