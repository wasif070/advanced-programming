{-# LANGUAGE GADTs #-} 

decToBin :: Int -> [Int]
decToBin n = listReverse (decToBinReverse n)

decToBinReverse :: Int -> [Int]
decToBinReverse 0 = []
decToBinReverse n = let (x, y) = divMod n 2 
                                in y : decToBinReverse x

listReverse :: [Int] -> [Int]
listReverse []     = []
listReverse (x:xs) = listReverse xs ++ [x]


decToBin' :: Int -> [Bool]
decToBin' 0 = [False]
decToBin' 1 = [True]
decToBin' n = let (x,y) = divMod n 2
                in (if even y then decToBin' x ++ [False]
                    else decToBin' x ++ [True]) 