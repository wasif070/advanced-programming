{-# LANGUAGE GADTs #-} 

{--
--Node labeled
data Tree a where
    Leaf :: Tree a 
    Node :: Tree a -> Tree a -> Tree a 
    deriving Show

-- Node labeled with value
data Tree a where
    Leaf :: a -> Tree a
    Node :: a -> Tree a -> Tree a -> Tree a 
    deriving Show

--Arbitrary leaf labeled
--}
--Define an abstract polymorphic data type Tree a to represent leaf-labeled binary trees 
--with labels of type a on the leaves.
data Tree a where
    Leaf :: a -> Tree a 
    Node :: Tree a -> Tree a -> Tree a 
    deriving Show

height     :: Tree a -> Int
height (Leaf _)                  = 1
height (Node leftTree rightTree) = 1 + max (height leftTree) (height rightTree)

leafNumber :: Tree a -> Int
leafNumber (Leaf _) = 1
leafNumber (Node leftTree rightTree) = leafNumber leftTree + leafNumber rightTree

--The flatTree function should flatten a tree of trees into a single tree.
flatTree   :: Tree (Tree a) -> Tree a
flatTree (Leaf t) = t
flatTree (Node leftTree rightTree) = Node (flatTree leftTree) (flatTree rightTree)

data List a where
    NoElem      :: List a 
    OneMoreElem :: a -> List a -> List a 
    deriving Show

(+++) :: List a -> List a -> List a 
NoElem +++ ys           = ys 
OneMoreElem x xs +++ ys = OneMoreElem x (xs +++ ys)    

treeToList :: Tree a -> List a
treeToList (Leaf n)                  = OneMoreElem n NoElem
treeToList (Node leftTree rightTree) = treeToList leftTree +++ treeToList rightTree

treeS :: Tree Int
treeS = Node (
         Node
            (Leaf 1)
            (Leaf 3)
        )
        (Leaf 7)
--labelTree numbers all leafs in a tree from left to right
-- ?

unzipTree :: Tree (a, b) -> (Tree a, Tree b)
unzipTree (Leaf (x,y))              = (Leaf x, Leaf y)
unzipTree (Node leftTree rightTree) = let (leftTree1, rightTree1) = unzipTree leftTree
                                          (leftTree2, rightTree2) = unzipTree rightTree
                                          in ((Node leftTree1 rightTree1), (Node leftTree2, rightTree2))
