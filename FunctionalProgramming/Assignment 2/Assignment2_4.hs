{-# LANGUAGE GADTs #-} 
import Prelude hiding (length)

data List a where
    NoElem :: List a
    OneMoreElem :: a -> List a -> List a
    deriving Show

data Tree a where 
    Node :: a -> List (Tree a) -> Tree a
    deriving Show

{-
   2
 / | \
7  3  1
   | / \
   0 3 2
-}
--
length :: List a -> Int
length NoElem             = 0
length (OneMoreElem _ xs) = 1 + length xs

leaf :: a -> Tree a
leaf x = Node x NoElem

tree1 :: Tree Int
tree1 = Node 2 (OneMoreElem (leaf 7)
                (OneMoreElem tree30
                 (OneMoreElem tre132 NoElem)))
    where
        tree30 :: Tree Int
        tree30 = Node 3 (OneMoreElem (leaf 0) NoElem)

        tre132 :: Tree Int
        tre132 = Node 1 (OneMoreElem (leaf 3) (OneMoreElem (leaf 2) NoElem))
                
tree2 :: Tree Int
tree2 = Node 8 (OneMoreElem tree1
                (OneMoreElem tree1 NoElem))

tree3 :: Tree Int
tree3 = Node 9 (OneMoreElem tree1
                (OneMoreElem tree1
                  (OneMoreElem tree1 NoElem)))

