{-# LANGUAGE GADTs #-} 

data Exp where
    Num :: Int -> Exp
    Add :: Exp -> Exp -> Exp
    Sub :: Exp -> Exp -> Exp
    Mul :: Exp -> Exp -> Exp
    Div :: Exp -> Exp -> Exp
    deriving Show

-- 4+5*6
exp1 :: Exp
exp1 =  Add (Num 4) (Mul (Num 5) (Num 6))

--4+5+6
exp2 :: Exp
exp2 = Add (Add (Num 4) (Num 5)) (Num 6)

--(4+3)*(5-2)
exp3 :: Exp
exp3 = Mul (Add (Num 4) (Num 3)) (Sub (Num 5) (Num 2))

--4+(4/(2-2))
exp4 :: Exp
exp4 = Add (Num 4) (Div (Num 4) (Sub (Num 2) (Num 2)))

eval :: Exp -> Int
eval (Num n)     = n
eval (Add e1 e2) = eval e1 + eval e2
eval (Sub e1 e2) = eval e1 - eval e2
eval (Mul e1 e2) = eval e1 * eval e2
eval (Div e1 e2) = div (eval e1) (eval e2)



evalS' :: Exp -> Maybe Int
evalS' (Num n) = Just n
evalS' (Add e1 e2) = case evalS' e1 of
                        Nothing -> Nothing
                        Just v1 -> case evalS' e2 of
                                    Nothing -> Nothing
                                    Just v2 -> Just (v1 +v2)
evalS' (Div e1 e2) = case evalS1' e2 of
                        Nothing -> Nothing
                        Just 0  -> Nothing
                        Just v2 -> case evalS1' e1 of
                                    Nothing -> Nothing
                                    Just v1 -> Just (v1 `div` v2)

evalS1' :: Exp -> Maybe Int
evalS1' (Num n) = Just n 
evalS1' (Add e1 e2) = helperEvalSAdd (evalS1' e1) (evalS1' e2)
                        where helperEvalSAdd :: Maybe Int -> Maybe Int -> Maybe Int
                              helperEvalSAdd Nothing Nothing   = Nothing
                              helperEvalSAdd (Just x) Nothing  = Nothing
                              helperEvalSAdd Nothing (Just y)  = Nothing
                              helperEvalSAdd (Just x) (Just y) = Just (x+y) 

evalS1' (Div e1 e2) = helperEvalSDiv (evalS1' e1) (evalS1' e2)
                        where helperEvalSDiv :: Maybe Int -> Maybe Int -> Maybe Int
                              helperEvalSDiv Nothing Nothing  = Nothing
                              helperEvalSDiv (Just 0) _       = Nothing                            
                              helperEvalSDiv (Just x) Nothing = Nothing
                              helperEvalSDiv Nothing (Just y) = Nothing
                              helperEvalSDiv (Just x) (Just y)= Just (x `div` y)

data Partial a where
    Undefined :: Partial a 
    Defined   :: a -> Partial a 
    deriving Show

evalS :: Exp -> Partial Int 
evalS (Num n)     = Defined n 
evalS (Add e1 e2) = case evalS e1 of
                        Undefined -> Undefined
                        Defined v1 -> case evalS e2 of
                                        Undefined -> Undefined
                                        Defined v2 -> Defined (v1 + v2)  
evalS (Sub e1 e2) = case evalS e1 of
                        Undefined -> Undefined
                        Defined v1 -> case evalS e2 of
                                        Undefined -> Undefined
                                        Defined v2 -> Defined (v1 - v2)

--Implement a function applyDistributivityLaw :: Exp -> Exp, which applies the rule (a+b)*c = a*c+b*c
--A good example might be the arithmetic expression (4+2)∗((1−3)∗4)

applyDistributivityLaw :: Exp -> Exp
applyDistributivityLaw (Mul e1 e2) = let e1' = applyDistributivityLaw e1
                                         e2' = applyDistributivityLaw e2
                                        in case e1' of 
                                            Add e1a e1b -> Add (Mul e1a e2') (Mul e1b e2')
                                            Sub e1a e1b -> Sub (Mul e1a e2') (Mul e1b e2')
                                            _           -> Mul e1' e2' 

applyDistributivityLaw (Div e1 e2) = let e1' = applyDistributivityLaw e1
                                         e2' = applyDistributivityLaw e2
                                         in case e1' of
                                            Add e1a e1b -> Add (Div e1a e2') (Div e1b e2')
                                            Sub e1a e1b -> Sub (Div e1a e2') (Div e1b e2')
                                            _           -> Mul e1' e2'

applyDistributivityLaw (Add e1 e2) = Add (applyDistributivityLaw e1) (applyDistributivityLaw e2)

applyDistributivityLaw (Sub e1 e2) = Sub (applyDistributivityLaw e1) (applyDistributivityLaw e2)

applyDistributivityLaw e           = e