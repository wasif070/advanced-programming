{-# LANGUAGE GADTs #-} 

data List a where
    NoElem      :: List a
    OneMoreElem :: a -> List a -> List a
  deriving Show
--merge (OneMoreElem 1 (OneMoreElem 2 NoElem))
--(OneMoreElem 3 (OneMoreElem 4 (OneMoreElem 5 NoElem)))
-- = OneMoreElem 1 (OneMoreElem 3 (OneMoreElem 2 (OneMoreElem 4 (OneMoreElem 5 NoElem))))
merge :: List a -> List a -> List a
merge NoElem ys = ys
merge xs NoElem = xs 
merge (OneMoreElem x xs) (OneMoreElem y ys) = OneMoreElem x (OneMoreElem y (merge xs ys))

--merge [1,2] [3,4,5] = [1,3,2,4,5]
merge' :: [Int] -> [Int] -> [Int]
merge' [] ys         = ys
merge' xs []         = xs
merge' (x:xs) (y:ys) = x : y : merge' xs ys

--intersperse 42 (OneMoreElem 1 (OneMoreElem 2 (OneMoreElem 3 NoElem)))
-- = OneMoreElem 1 (OneMoreElem 42 (OneMoreElem 2 (OneMoreElem 42 (OneMoreElem 3 NoElem))))
intersperse :: a -> List a -> List a
intersperse _ NoElem                 = NoElem
intersperse _ (OneMoreElem y NoElem) = OneMoreElem y NoElem
intersperse x (OneMoreElem y ys)     = OneMoreElem y (OneMoreElem x (intersperse x ys))

--splitAt 2 (OneMoreElem 1 (OneMoreElem 2 (OneMoreElem 3 (OneMoreElem 4 (OneMoreElem 5 NoElem)))))
--  = (OneMoreElem 1 (OneMoreElem 2 NoElem), OneMoreElem 3 (OneMoreElem 4 (OneMoreElem 5 NoElem)))
splitAt' :: Int -> List a -> (List a, List a)
splitAt' 0 xs                 = (NoElem, xs)
splitAt' _ NoElem             = (NoElem, NoElem)
--splitAt' n (OneMoreElem x xs) = let (as, bs) = splitAt' (n - 1) xs
--                                in (OneMoreElem x as, bs)
splitAt' n (OneMoreElem x xs) = (OneMoreElem x as, bs)
                                where (as, bs) = splitAt' (n-1) xs

--extractLefts (OneMoreElem (Left 42) (OneMoreElem (Right True) NoElem))
-- = OneMoreElem 42 NoElem
extractLefts :: List (Either a b) -> List a
extractLefts NoElem                     = NoElem
extractLefts (OneMoreElem (Left x) xs)  = OneMoreElem x (extractLefts xs)
extractLefts (OneMoreElem (Right _) xs) = extractLefts xs
