{-# LANGUAGE GADTs #-} 

data List a where
    NoElem      :: List a
    OneMoreElem :: a -> List a -> List a
    deriving Show

oneList :: List Int
oneList = OneMoreElem 1 (OneMoreElem 2 (OneMoreElem 3 NoElem))

oneConvenientList1 :: [Int]
oneConvenientList1 =  1:(2:(3:[]))

oneConvenientList2 :: [Int]
oneConvenientList2 = [1,2,3]

data Partial a where
    Defined   :: a -> Partial a
    Undefined :: Partial a
    deriving Show

headPartial :: [a] -> Partial a
headPartial []     = Undefined
headPartial (x:xs) = Defined x

tailPartial :: [a] -> Partial [a]
tailPartial []     = Undefined
tailPartial (x:xs) = Defined xs

(+++) :: [a] -> [a] -> [a]
[] +++ ys     = ys
(x:xs) +++ ys = x : (xs +++ ys)  

reverseList :: [Int] -> [Int]
reverseList []     = []
reverseList (x:xs) = reverseList xs +++ [x]

--[1,2,3] = [2,3,4]
incList :: [Int] -> [Int]
incList [] = []
--incList xs = [1+x | x <- xs] 
incList (x:xs) = (1+x) : incList xs

--[True,False] = [False,True]
negateList :: [Bool] -> [Bool]
negateList []     = []
negateList (x:xs) = not x : negateList xs

--[1,2,3] = 6
sumOfInts :: [Int] -> Int
sumOfInts [] = 0
sumOfInts (x:xs) = x + sumOfInts xs

--[True,False] = False
sumOfBoolsAnd :: [Bool] -> Bool
sumOfBoolsAnd [] = True
sumOfBoolsAnd (x:xs) = x && sumOfBoolsAnd xs

--[True,False] = True
sumOfBoolsOr :: [Bool] -> Bool
sumOfBoolsOr []     = False 
sumOfBoolsOr (x:xs) = x || sumOfBoolsOr xs

doNotInfer = Undefined

undefinedBool :: Partial Bool
undefinedBool = Undefined

undefinedInt :: Partial Int
undefinedInt = Undefined

isUndefinedInt :: Partial Int -> Bool
isUndefinedInt Undefined   = True
isUndefinedInt (Defined x) = False

isUndefined :: Partial a -> Bool
isUndefined Undefined   = True
isUndefined (Defined x) = False

data Suit where
    Diamonds :: Suit
    Hearts   :: Suit
    Clubs    :: Suit
    Spades   :: Suit
 deriving Show

valSuit1 :: Suit
valSuit1 = Diamonds

valSuit2 :: Suit
valSuit2 = Hearts

valSuit3 :: Suit
valSuit3 = Clubs

valSuit4 :: Suit
valSuit4 = Spades

isRed :: Suit -> Bool
isRed Diamonds = True
isRed Hearts   = True
isRed _        = False

isBlack :: Suit -> Bool
isBlack suit = not (isRed suit)

data Simple where
    Easy             :: Simple
    NotQuiteThatEasy :: Bool -> Simple
    deriving Show

valSimple1 :: Simple
valSimple1 = Easy

valSimple2 :: Simple
valSimple2 = NotQuiteThatEasy True

valSimple3 :: Simple
valSimple3 = NotQuiteThatEasy False

valSimple2' :: Simple
valSimple2' = NotQuiteThatEasy (True || False)

data Pair a b where
    Pair :: a -> b -> Pair a b 
    deriving Show

valPairSuitBool1 :: Pair Suit Bool
valPairSuitBool1 = Pair Diamonds True

valPairSuitBool2 :: Pair Suit Bool
valPairSuitBool2 = Pair Diamonds False

valPairSuitBool3 :: Pair Suit Bool
valPairSuitBool3 = Pair Hearts True

valPairSuitBool4 :: Pair Suit Bool
valPairSuitBool4 = Pair Hearts False

valPairSuitBool5 :: Pair Suit Bool
valPairSuitBool5 = Pair Clubs True

valPairSuitBool6 :: Pair Suit Bool
valPairSuitBool6 = Pair Clubs False

valPairSuitBool7 :: Pair Suit Bool
valPairSuitBool7 = Pair Spades True

valPairSuitBool8 :: Pair Suit Bool
valPairSuitBool8 = Pair Spades False

{--
data Either a b where
    Left  :: a -> Either a b
    Right :: b -> Either a b
--}

valEitherSuitBool1 :: Either Suit Bool
valEitherSuitBool1 = Left Diamonds