module SimplePrelude (
  -- * Standard types and related functions

  -- ** Basic data types
  Bool(False, True),
  (&&), (||), not, otherwise,

  Maybe(Nothing, Just),
  maybe,

  Either(Left, Right),
  either,

  Char, chr, ord,
  String,

  -- *** Tuples
  fst, snd, curry, uncurry,

  (==), (/=),
  (==.), (/=.),
  (<), (<=), (>=), (>), max, min,
  (<.), (<=.), (>=.), (>.), maxF, minF,

  -- ** Numbers

  -- *** Numeric types
  Int, Float,

  -- *** Arithmetic operations
  (+), (-), (*), abs, signum,
  (+.), (-.), (*.), (/.), absF, signumF,
  quot, rem, div, mod, quotRem, divMod,
  pi, exp, log, sqrt, (**), logBase, sin, cos, tan,
  asin, acos, atan, sinh, cosh, tanh, asinh, acosh, atanh,
  truncate, round, ceiling, floor,

  -- *** Numeric functions
  even, odd, gcd, lcm, (^), (^^),
    
  -- *** Conversion functions
  intToFloat, floatToInt,

  -- * Basic Input and output IO monad
  IO, (>>=), (>>), return, fail,
  mapIO, mapIO_, sequenceIO, sequenceIO_,
  -- ** Simple I\/O operations
  -- All I/O functions defined here are character oriented.  The
  -- treatment of the newline character will vary on different systems.
  -- For example, two characters of input, return and linefeed, may
  -- read as a single newline character.  These functions cannot be
  -- used portably for binary I/O.
  -- *** Output functions
  putChar,
  putStr, putStrLn,
  -- *** Input functions
  getChar,
  getLine,


  -- ** Folding of lists
  foldr, foldl, foldr1, foldl1,
  maximum, maximumF,
  minimum, minimumF,
  product, productF,
  sum, sumF,

  -- ** Miscellaneous functions
  id, const, (.), flip, ($), until,
  error, undefined,

  -- * List operations
  map, (++), filter,
  head, last, tail, init, null, length, (!!),
  reverse,
  -- *** Special folds
  and, or, any, all,
  concat, concatMap,
  -- ** Building lists
  -- *** Infinite lists
  iterate, repeat, replicate, cycle,
  -- ** Sublists
  take, drop, splitAt, takeWhile, dropWhile, span, break,
  -- ** Zipping and unzipping lists
  zip, zip3, zipWith, zipWith3, unzip, unzip3,
  -- ** Functions on strings
  lines, words, unlines, unwords,

  -- * Converting to @String@
  Show, showInt, showFloat
  ) where

import qualified ThisModuleIsOnlyForTheSimplePrelude as P

import ThisModuleIsOnlyForTheSimplePrelude
  ( Bool (..), Char, Either (..), Float, Int, Integer, IO, Maybe (..), String
  , Rational, Show (show)
  )

import Prelude (fromInteger, fromRational, ifThenElse, negate)

infixr 9  .
infixr 8  ^, ^^, **
infixl 7  *, *., /.
infixl 6  +, +., -, -.
infixr 5  ++
infix  4  ==, ==., /=, /=., <, <., <=, <=., >, >., >=, >=.
infixr 3  &&
infixr 2  ||
infixl 1  >>, >>=
infixr 0  $

-- | Boolean "and"
(&&) :: Bool -> Bool -> Bool
(&&) = (P.&&)

-- | Boolean "or"
(||) :: Bool -> Bool -> Bool
(||) = (P.||)

-- | Boolean "not"
not :: Bool -> Bool
not = P.not

-- | 'otherwise' is defined as the value 'True'.  It helps to make
-- guards more readable.  eg.
--
-- >  f x | x < 0     = ...
-- >      | otherwise = ...
otherwise :: Bool
otherwise = P.otherwise

-- | The 'maybe' function takes a default value, a function, and a 'Maybe'
-- value.  If the 'Maybe' value is 'Nothing', the function returns the
-- default value.  Otherwise, it applies the function to the value inside
-- the 'Just' and returns the result.
--
-- ==== __Examples__
--
-- Basic usage:
--
-- >>> maybe False odd (Just 3)
-- True
--
-- >>> maybe False odd Nothing
-- False
--
maybe :: b -> (a -> b) -> Maybe a -> b
maybe = P.maybe

-- | Case analysis for the 'Either' type.
-- If the value is @'Left' a@, apply the first function to @a@;
-- if it is @'Right' b@, apply the second function to @b@.
--
-- ==== __Examples__
--
-- We create two values of type @'Either' 'String' 'Int'@, one using the
-- 'Left' constructor and another using the 'Right' constructor. Then
-- we apply \"either\" the 'length' function (if we have a 'String')
-- or the \"times-two\" function (if we have an 'Int'):
--
-- >>> let s = Left "foo" :: Either String Int
-- >>> let n = Right 3 :: Either String Int
-- >>> either length (*2) s
-- 3
-- >>> either length (*2) n
-- 6
--
either :: (a -> c) -> (b -> c) -> Either a b -> c
either = P.either

-- | Extract the first component of a pair.
fst :: (a, b) -> a
fst = P.fst

-- | Extract the second component of a pair.
snd :: (a, b) -> b
snd = P.snd

-- | 'curry' converts an uncurried function to a curried function.
--
-- ==== __Examples__
--
-- >>> curry fst 1 2
-- 1
curry :: ((a, b) -> c) -> a -> b -> c
curry = P.curry

-- | 'uncurry' converts a curried function to a function on pairs.
--
-- ==== __Examples__
--
-- >>> uncurry (+) (1,2)
-- 3
--
-- >>> map (uncurry max) [(1,2), (3,4), (6,8)]
-- [2,4,8]
uncurry :: (a -> b -> c) -> (a, b) -> c
uncurry = P.uncurry

-- Conversion between 'Char's and 'Int's

ord :: Char -> Int
ord = P.ord

chr :: Int -> Char
chr = P.chr

-- Functions on 'Int's

-- | Equality on 'Int'
(==) :: Int -> Int -> Bool
(==) = (P.==)

-- | Inequality on 'Int'
(/=) :: Int -> Int -> Bool
(/=) = (P./=)

-- | integer division truncated toward zero
quot :: Int -> Int -> Int
quot = P.quot

-- | integer remainder, satisfying
--
-- > (x `quot` y)*y + (x `rem` y) evaluates to x
rem :: Int -> Int -> Int
rem = P.rem

-- | integer division truncated toward negative infinity
div :: Int -> Int -> Int
div = P.div

-- | integer modulus, satisfying
--
-- > (x `div` y)*y + (x `mod` y) evaluates to x
mod :: Int -> Int -> Int
mod = P.mod

-- | simultaneous 'quot' and 'rem'
quotRem :: Int -> Int -> (Int, Int)
quotRem = P.quotRem

-- | simultaneous 'div' and 'mod'
divMod :: Int -> Int -> (Int, Int)
divMod = P.divMod

-- | Addition on 'Int'
(+) :: Int -> Int -> Int
(+) = (P.+)

-- | Subtraction on 'Int'
(-) :: Int -> Int -> Int
(-) = (P.-)

-- | Multiplication on 'Int'
(*) :: Int -> Int -> Int
(*) = (P.*)

-- | Absolute value of an 'Int'
abs :: Int -> Int
abs = P.abs

-- | Sign of an 'Int'
signum :: Int -> Int
signum = P.signum

-- | Convert an 'Int' to a 'Float'
intToFloat :: Int -> Float
intToFloat = P.fromIntegral

-- | Convert an 'Int' to a 'String'
showInt :: Int -> String
showInt = P.show

-- | Less comparison on 'Int'
(<) :: Int -> Int -> Bool
(<) = (P.<)

-- | Less or equal comparison on 'Int'
(<=) :: Int -> Int -> Bool
(<=) = (P.<=)

-- | Greater comparison on 'Int'
(>) :: Int -> Int -> Bool
(>) = (P.>)

-- | Greater or equal comparison on 'Int'
(>=) :: Int -> Int -> Bool
(>=) = (P.>=)

-- | Maximum of two 'Int's
max :: Int -> Int -> Int
max = P.max

-- | Minimum of two 'Int's
min :: Int -> Int -> Int
min = P.min

-- | Check if an 'Int' is even
even :: Int -> Bool
even = P.even

-- | Check if an 'Int' is odd
odd :: Int -> Bool
odd = P.odd

-- | @'gcd' x y@ is the non-negative factor of both @x@ and @y@ of which
-- every common factor of @x@ and @y@ is also a factor; for example
-- @'gcd' 4 2 = 2@, @'gcd' (-4) 6 = 2@, @'gcd' 0 4@ = @4@. @'gcd' 0 0@ = @0@.
-- (That is, the common divisor that is \"greatest\" in the divisibility
-- preordering.)
gcd :: Int -> Int -> Int
gcd = P.gcd

-- | @'lcm' x y@ is the smallest positive integer that both @x@ and @y@ divide.
lcm :: Int -> Int -> Int
lcm = P.lcm

-- | raise an integer number to a non-negative integer power
(^) :: Int -> Int -> Int
(^) = (P.^)

-- | raise a floating point number to an integer power
(^^) :: Float -> Int -> Float
(^^) = (P.^^)

-- Functions on 'Float's

-- | Equality on 'Float'
(==.) :: Float -> Float -> Bool
(==.) = (P.==)

-- | Inequality on 'Float'
(/=.) :: Float -> Float -> Bool
(/=.) = (P./=)

pi :: Float
pi = P.pi

exp :: Float -> Float
exp = P.exp

log :: Float -> Float
log = P.log

sqrt :: Float -> Float
sqrt = P.sqrt

(**) :: Float -> Float -> Float
(**) = (P.**)

logBase :: Float -> Float -> Float
logBase = P.logBase

sin :: Float -> Float
sin = P.sin

cos :: Float -> Float
cos = P.cos

tan :: Float -> Float
tan = P.tan

asin :: Float -> Float
asin = P.asin

acos :: Float -> Float
acos = P.acos

atan :: Float -> Float
atan = P.atan

sinh :: Float -> Float
sinh = P.sinh

cosh :: Float -> Float
cosh = P.cosh

tanh :: Float -> Float
tanh = P.tanh

asinh :: Float -> Float
asinh = P.asinh

acosh :: Float -> Float
acosh = P.acosh

atanh :: Float -> Float
atanh = P.atanh

-- | Division on 'Float'
(/.) :: Float -> Float -> Float
(/.) = (P./)

-- | Addition on 'Float'
(+.) :: Float -> Float -> Float
(+.) = (P.+)

-- | Subtraction on 'Float'
(-.) :: Float -> Float -> Float
(-.) = (P.-)

-- | Multiplication on 'Float'
(*.) :: Float -> Float -> Float
(*.) = (P.*)

-- | Absolute value of a 'Float'
absF :: Float -> Float
absF = P.abs

-- | Sign of a 'Float'
signumF :: Float -> Float
signumF = P.signum

-- | Convert a 'Float' to an 'Int'
floatToInt :: Float -> Int
floatToInt = P.round

-- | Convert a 'Float' to a 'String'
showFloat :: Float -> String
showFloat = P.show

-- | Less comparison on 'Float'
(<.) :: Float -> Float -> Bool
(<.) = (P.<)

-- | Less or equal comparison on 'Float'
(<=.) :: Float -> Float -> Bool
(<=.) = (P.<=)

-- | Greater comparison on 'Float'
(>.) :: Float -> Float -> Bool
(>.) = (P.>)

-- | Greater or equal comparison on 'Float'
(>=.) :: Float -> Float -> Bool
(>=.) = (P.>=)

-- | Maximum of two 'Float's
maxF :: Float -> Float -> Float
maxF = P.max

-- | Minimum of two 'Float's
minF :: Float -> Float -> Float
minF = P.min

-- | @'truncate' x@ returns the integer nearest @x@ between zero and @x@
truncate :: Float -> Int
truncate = P.truncate

-- | @'round' x@ returns the nearest integer to @x@;
--   the even integer if @x@ is equidistant between two integers
round :: Float -> Int
round = P.round

-- | @'ceiling' x@ returns the least integer not less than @x@
ceiling :: Float -> Int
ceiling = P.ceiling

-- | @'floor' x@ returns the greatest integer not greater than @x@
floor :: Float -> Int
floor = P.floor

-- Miscellaneous functions

-- | Identity function.
--
-- > id x = x
id :: a -> a
id = P.id

-- | @const x@ is a unary function which evaluates to @x@ for all inputs.
--
-- >>> const 42 "hello"
-- 42
--
-- >>> map (const 42) [0..3]
-- [42,42,42,42]
const :: a -> b -> a
const = P.const

-- | Function composition.
(.) :: (b -> c) -> (a -> b) -> a -> c
(.) = (P..)

-- | @'flip' f@ takes its (first) two arguments in the reverse order of @f@.
--
-- >>> flip (++) "hello" "world"
-- "worldhello"
flip :: (a -> b -> c) -> b -> a -> c
flip = P.flip

-- | Application operator.  This operator is redundant, since ordinary
-- application @(f x)@ means the same as @(f '$' x)@. However, '$' has
-- low, right-associative binding precedence, so it sometimes allows
-- parentheses to be omitted; for example:
--
-- > f $ g $ h x  =  f (g (h x))
--
-- It is also useful in higher-order situations, such as @'map' ('$' 0) xs@,
-- or @'zipWith' ('$') fs xs@.
--
-- Note that @($)@ is levity-polymorphic in its result type, so that
--     foo $ True    where  foo :: Bool -> Int#
-- is well-typed
($) :: (a -> b) -> a -> b
($) = (P.$)

-- | @'until' p f@ yields the result of applying @f@ until @p@ holds.
until :: (a -> Bool) -> (a -> a) -> a -> a
until = P.until

-- | 'error' stops execution and displays an error message.
error :: String -> a
error = P.error

-- | A special case of 'error'.
-- It is expected that compilers will recognize this and insert error
-- messages which are more appropriate to the context in which 'undefined'
-- appears.
undefined :: a
undefined = P.undefined

-- List Operations

-- | 'map' @f xs@ is the list obtained by applying @f@ to each element
-- of @xs@, i.e.,
--
-- > map f [x1, x2, ..., xn] evaluates to [f x1, f x2, ..., f xn]
-- > map f [x1, x2, ...] evaluates to [f x1, f x2, ...]
map :: (a -> b) -> [a] -> [b]
map = P.map

-- | Append two lists, i.e.,
--
-- > [x1, ..., xm] ++ [y1, ..., yn] evaluates to [x1, ..., xm, y1, ..., yn]
-- > [x1, ..., xm] ++ [y1, ...] evaluates to [x1, ..., xm, y1, ...]
--
-- If the first list is not finite, the result is the first list.
(++) :: [a] -> [a] -> [a]
(++) = (P.++)

-- | 'filter', applied to a predicate and a list, returns the list of
-- those elements that satisfy the predicate; i.e.,
--
-- > filter p xs = [ x | x <- xs, p x]
filter :: (a -> Bool) -> [a] -> [a]
filter = P.filter

-- | Extract the first element of a list, which must be non-empty.
head :: [a] -> a
head = P.head

-- | Extract the last element of a list, which must be finite and non-empty.
last :: [a] -> a
last = P.last

-- | Extract the elements after the head of a list, which must be non-empty.
tail :: [a] -> [a]
tail = P.tail

-- | Return all the elements of a list except the last one.
-- The list must be non-empty.
init :: [a] -> [a]
init = P.init

-- | Test whether a list is empty.
null :: [a] -> Bool
null = P.null

-- | Returns the length of a list as an 'Int'.
length :: [a] -> Int
length = P.length

-- | List index (subscript) operator, starting from 0.
(!!) :: [a] -> Int -> a
(!!) = (P.!!)

-- | 'reverse' @xs@ returns the elements of @xs@ in reverse order.
-- @xs@ must be finite.
reverse :: [a] -> [a]
reverse = P.reverse

-- | Left-associative fold of a list
--
-- Reduces the list using the binary operator, from left to right:
--
-- > foldl f z [x1, x2, ..., xn] evaluates to 
--   (...((z `f` x1) `f` x2) `f`...) `f` xn
--
-- Note that to produce the outermost application of the operator the
-- entire input list must be traversed. This means that 'foldl'' will
-- diverge if given an infinite list.
foldl :: (b -> a -> b) -> b -> [a] -> b
foldl = P.foldl

-- | Right-associative fold of a list.
--
-- Reduces the list using the binary operator, from right to left:
--
-- > foldr f z [x1, x2, ..., xn] evaluates to x1 `f` (x2 `f` ... (xn `f` z)...)
--
-- Note that, since the head of the resulting expression is produced by
-- an application of the operator to the first element of the list,
-- 'foldr' can produce a terminating expression from an infinite list.
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr = P.foldr

-- | A variant of 'foldr' that has no base case,
-- and thus may only be applied to non-empty lists.
foldr1 :: (a -> a -> a) -> [a] -> a
foldr1 = P.foldr1

-- | A variant of 'foldl' that has no base case,
-- and thus may only be applied to non-empty lists.
foldl1 :: (a -> a -> a) -> [a] -> a
foldl1 = P.foldl1

-- | 'and' returns the conjunction of a list of Bools.  For the
-- result to be 'True', the list must be finite; 'False', however,
-- results from a 'False' value finitely far from the left end.
and :: [Bool] -> Bool
and = P.and

-- | 'or' returns the disjunction of a list of Bools.  For the
-- result to be 'False', the list must be finite; 'True', however,
-- results from a 'True' value finitely far from the left end.
or :: [Bool] -> Bool
or = P.or

-- | Determines whether any element of the list satisfies the predicate.
any :: (a -> Bool) -> [a] -> Bool
any = P.any

-- | Determines whether all elements of the list satisfy the predicate.
all :: (a -> Bool) -> [a] -> Bool
all = P.all

-- | Computes the sum of a list of integer numbers
sum :: [Int] -> Int
sum = P.sum

-- | Computes the product of a list of integer numbers
product :: [Int] -> Int
product = P.product

-- | Computes the maximum of a non-empty list of integer numbers
maximum :: [Int] -> Int
maximum = P.maximum

-- | Computes the minimum of a non-empty list of integer numbers
minimum :: [Int] -> Int
minimum = P.minimum

-- | Computes the sum of a list of floating point numbers
sumF :: [Float] -> Float
sumF = P.sum

-- | Computes the product of a list of floating point numbers
productF :: [Float] -> Float
productF = P.product

-- | Computes the maximum of a non-empty list of floating point numbers
maximumF :: [Float] -> Float
maximumF = P.maximum

-- | Computes the minimum of a non-empty list of floating point numbers
minimumF :: [Float] -> Float
minimumF = P.minimum

-- | The concatenation of all the elements of a list of lists.
concat :: [[a]] -> [a]
concat = P.concat

-- | Map a function over all the elements of a list and concatenate
-- the resulting lists.
concatMap :: (a -> [b]) -> [a] -> [b]
concatMap = P.concatMap

-- | 'iterate' @f x@ returns an infinite list of repeated applications
-- of @f@ to @x@:
--
-- > iterate f x evaluates to [x, f x, f (f x), ...]
--
-- Note that 'iterate' is lazy, potentially leading to thunk build-up if
-- the consumer doesn't force each iterate.
iterate :: (a -> a) -> a -> [a]
iterate = P.iterate

-- | 'repeat' @x@ is an infinite list, with @x@ the value of every element.
repeat :: a -> [a]
repeat = P.repeat

-- | 'replicate' @n x@ is a list of length @n@ with @x@ the value of
-- every element.
replicate :: Int -> a -> [a]
replicate = P.replicate

-- | 'cycle' ties a finite list into a circular one, or equivalently,
-- the infinite repetition of the original list. It is the identity
-- on infinite lists.
cycle :: [a] -> [a]
cycle = P.cycle

-- | 'take' @n@, applied to a list @xs@, returns the prefix of @xs@
-- of length @n@, or @xs@ itself if @n > 'length' xs@:
--
-- > take 5 "Hello World!" evaluates to "Hello"
-- > take 3 [1,2,3,4,5] evaluates to [1,2,3]
-- > take 3 [1,2] evaluates to [1,2]
-- > take 3 [] evaluates to []
-- > take (-1) [1,2] evaluates to []
-- > take 0 [1,2] evaluates to []
--
take :: Int -> [a] -> [a]
take = P.take

-- | 'drop' @n xs@ returns the suffix of @xs@
-- after the first @n@ elements, or @[]@ if @n > 'length' xs@:
--
-- > drop 6 "Hello World!" evaluates to "World!"
-- > drop 3 [1,2,3,4,5] evaluates to [4,5]
-- > drop 3 [1,2] evaluates to []
-- > drop 3 [] evaluates to []
-- > drop (-1) [1,2] evaluates to [1,2]
-- > drop 0 [1,2] evaluates to [1,2]
--
drop :: Int -> [a] -> [a]
drop = P.drop

-- | 'splitAt' @n xs@ returns a tuple where first element is @xs@ prefix of
-- length @n@ and second element is the remainder of the list:
--
-- > splitAt 6 "Hello World!" evaluates to ("Hello ","World!")
-- > splitAt 3 [1,2,3,4,5] evaluates to ([1,2,3],[4,5])
-- > splitAt 1 [1,2,3] evaluates to ([1],[2,3])
-- > splitAt 3 [1,2,3] evaluates to ([1,2,3],[])
-- > splitAt 4 [1,2,3] evaluates to ([1,2,3],[])
-- > splitAt 0 [1,2,3] evaluates to ([],[1,2,3])
-- > splitAt (-1) [1,2,3] evaluates to ([],[1,2,3])
--
splitAt :: Int -> [a] -> ([a], [a])
splitAt = P.splitAt

-- | 'takeWhile', applied to a predicate @p@ and a list @xs@, returns the
-- longest prefix (possibly empty) of @xs@ of elements that satisfy @p@:
--
-- > takeWhile (< 3) [1,2,3,4,1,2,3,4] evaluates to [1,2]
-- > takeWhile (< 9) [1,2,3] evaluates to [1,2,3]
-- > takeWhile (< 0) [1,2,3] evaluates to []
--
takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile = P.takeWhile

-- | 'dropWhile' @p xs@ returns the suffix remaining after 'takeWhile' @p xs@:
--
-- > dropWhile (< 3) [1,2,3,4,5,1,2,3] evaluates to [3,4,5,1,2,3]
-- > dropWhile (< 9) [1,2,3] evaluates to []
-- > dropWhile (< 0) [1,2,3] evaluates to [1,2,3]
--
dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile = P.dropWhile

-- | 'span', applied to a predicate @p@ and a list @xs@, returns a tuple where
-- first element is longest prefix (possibly empty) of @xs@ of elements that
-- satisfy @p@ and second element is the remainder of the list:
--
-- > span (< 3) [1,2,3,4,1,2,3,4] evaluates to ([1,2],[3,4,1,2,3,4])
-- > span (< 9) [1,2,3] evaluates to ([1,2,3],[])
-- > span (< 0) [1,2,3] evaluates to ([],[1,2,3])
--
-- 'span' @p xs@ is equivalent to @('takeWhile' p xs, 'dropWhile' p xs)@
span :: (a -> Bool) -> [a] -> ([a], [a])
span = P.span

-- | 'break', applied to a predicate @p@ and a list @xs@, returns a tuple where
-- first element is longest prefix (possibly empty) of @xs@ of elements that
-- /do not satisfy/ @p@ and second element is the remainder of the list:
--
-- > break (> 3) [1,2,3,4,1,2,3,4] evaluates to ([1,2,3],[4,1,2,3,4])
-- > break (< 9) [1,2,3] evaluates to ([],[1,2,3])
-- > break (> 9) [1,2,3] evaluates to ([1,2,3],[])
--
-- 'break' @p@ is equivalent to @'span' ('not' . p)@.
break :: (a -> Bool) -> [a] -> ([a], [a])
break = P.break

-- | 'zip' takes two lists and returns a list of corresponding pairs.
--
-- > zip [1, 2] ['a', 'b'] evaluates to [(1, 'a'), (2, 'b')]
--
-- If one input list is short, excess elements of the longer list are
-- discarded:
--
-- > zip [1] ['a', 'b'] evaluates to [(1, 'a')]
-- > zip [1, 2] ['a'] evaluates to [(1, 'a')]
--
-- 'zip' is right-lazy:
--
-- > zip [] _|_ = []
-- > zip _|_ [] = _|_
zip :: [a] -> [b] -> [(a, b)]
zip = P.zip

-- | 'zip3' takes three lists and returns a list of triples, analogous to
-- 'zip'.
zip3 :: [a] -> [b] -> [c] -> [(a, b, c)]
zip3 = P.zip3

-- | 'zipWith' generalises 'zip' by zipping with the function given
-- as the first argument, instead of a tupling function.
-- For example, @'zipWith' (+)@ is applied to two lists to produce the
-- list of corresponding sums.
--
-- 'zipWith' is right-lazy:
--
-- > zipWith f [] _|_ = []
zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith = P.zipWith

-- | The 'zipWith3' function takes a function which combines three
-- elements, as well as three lists and returns a list of their point-wise
-- combination, analogous to 'zipWith'.
zipWith3 :: (a -> b -> c -> d) -> [a] -> [b] -> [c] -> [d]
zipWith3 = P.zipWith3

-- | 'unzip' transforms a list of pairs into a list of first components
-- and a list of second components.
unzip :: [(a, b)] -> ([a], [b])
unzip = P.unzip

-- | The 'unzip3' function takes a list of triples and returns three
-- lists, analogous to 'unzip'.
unzip3 :: [(a, b, c)] -> ([a], [b], [c])
unzip3 = P.unzip3

-- | 'lines' breaks a string up into a list of strings at newline
-- characters.  The resulting strings do not contain newlines.
--
-- Note that after splitting the string at newline characters, the
-- last part of the string is considered a line even if it doesn't end
-- with a newline. For example,
--
-- >>> lines ""
-- []
--
-- >>> lines "\n"
-- [""]
--
-- >>> lines "one"
-- ["one"]
--
-- >>> lines "one\n"
-- ["one"]
--
-- >>> lines "one\n\n"
-- ["one",""]
--
-- >>> lines "one\ntwo"
-- ["one","two"]
--
-- >>> lines "one\ntwo\n"
-- ["one","two"]
--
-- Thus @'lines' s@ contains at least as many elements as newlines in @s@.
lines :: String -> [String]
lines = P.lines

-- | 'words' breaks a string up into a list of words, which were delimited
-- by white space.
--
-- >>> words "Lorem ipsum\ndolor"
-- ["Lorem","ipsum","dolor"]
words :: String -> [String]
words = P.words

-- | 'unlines' is an inverse operation to 'lines'.
-- It joins lines, after appending a terminating newline to each.
--
-- >>> unlines ["Hello", "World", "!"]
-- "Hello\nWorld\n!\n"
unlines :: [String] -> String
unlines = P.unlines

-- | 'unwords' is an inverse operation to 'words'.
-- It joins words with separating spaces.
--
-- >>> unwords ["Lorem", "ipsum", "dolor"]
-- "Lorem ipsum dolor"
unwords :: [String] -> String
unwords = P.unwords

-- IO

-- | Sequentially compose two 'IO' actions, passing any value produced by
--   the first as an argument to the second.
(>>=) :: IO a -> (a -> IO b) -> IO b
(>>=) = (P.>>=)

-- | Sequentially compose two 'IO' actions, discarding any value produced by
--   the first, like sequencing operators (such as the semicolon) in imperative
--   languages.
(>>) :: IO a -> IO b -> IO b
(>>) = (P.>>)

-- | Inject a value into the 'IO' type.
return :: a -> IO a
return = P.return

-- | Fail with a message.
--   This operation is not part of the mathematical definition of a monad,
--   but is invoked on pattern-match failure in a do expression.
fail :: String -> IO a
fail = P.fail

-- | Map each element of a list to an 'IO' action, evaluate
-- these actions from left to right, and collect the results. For
-- a version that ignores the results see 'mapIO_'.
mapIO :: (a -> IO b) -> [a] -> IO [b]
mapIO = P.mapM

-- | Map each element of a list to an 'IO' action, evaluate
-- these actions from left to right, and ignore the results. For a
-- version that doesn't ignore the results see 'mapIO'.
mapIO_ :: (a -> IO b) -> [a] -> IO ()
mapIO_ = P.mapM_

-- | Evaluate each 'IO' action in the list from left to
-- right, and collect the results. For a version that ignores the
-- results see 'sequenceIO_'.
sequenceIO :: [IO a] -> IO [a]
sequenceIO = P.sequence

-- | Evaluate each 'IO' action in the list from left to right,
-- and ignore the results. For a version that doesn't ignore the
-- results see 'sequenceIO'.
sequenceIO_ :: [IO a] -> IO ()
sequenceIO_ = P.sequence_

-- | Write a character to the standard output device
putChar :: Char -> IO ()
putChar = P.putChar

-- | Write a string to the standard output device
putStr :: String -> IO ()
putStr = P.putStr

-- | The same as 'putStr', but adds a newline character.
putStrLn :: String -> IO ()
putStrLn = P.putStrLn

-- | Read a character from the standard input device
getChar :: IO Char
getChar = P.getChar

-- | Read a line from the standard input device
getLine :: IO String
getLine = P.getLine
