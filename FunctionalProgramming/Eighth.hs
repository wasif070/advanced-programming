{-# LANGUAGE GADTs #-} 
import Data.Char (toUpper)
import System.IO

double :: Int -> Int
double n = n * 2

isDigit :: Char -> Bool
isDigit '0' = True
isDigit '1' = True
isDigit '2' = True
isDigit '3' = True
isDigit '4' = True
isDigit '5' = True
isDigit '6' = True
isDigit '7' = True
isDigit '8' = True
isDigit '9' = True
isDigit _   = False

isDigit' :: Char -> Bool
isDigit' n = elem n "0123456789"

isNumber :: String -> Bool
isNumber [] = False
isNumber numStr = foldr(\chr accBool -> isDigit chr && accBool) True numStr 

getInt :: IO Int
getInt = getLine >>= \str ->
    if isNumber str
        then return (read str)
    else putStrLn "False Input, please type an actual value"
        >>getInt

getDouble :: IO ()
getDouble = putStrLn "Please type a numeric value!"
          >> getInt
          >>= (\num ->
                putStr "Doubled value:"
                >>print (double num))

getMultiplication :: IO ()
getMultiplication = putStrLn "Please type numeric value!"
                  >> getInt
                  >>= (\num1 ->
                       putStrLn "Please type a numeric value"
                       >> getInt
                       >>= (\num2 -> 
                           putStr "Multiplication:"
                           >>print(num1 * num2)))

getReverseAndMore :: IO ()
getReverseAndMore = putStrLn "Please type a word!"
                    >> getLine
                    >>= (\str -> 
                          putStr "Reversed word:"
                          >>putStr (reverse str)
                          >>putStr "\n"
                          >>putStr "Head:"
                          >>putChar (head str)
                          >>putStr "\n"
                          >>putStr "Last:"
                          >>putChar (last str)
                          >>putStr "\n"
                          )

isNewLine :: Char -> Bool
isNewLine '\n' = True
isNewLine _    = False

firstToUpper :: String -> String
firstToUpper [] = []
firstToUpper (c:str) = toUpper c : firstToUpper str

upperCasesInFile :: FilePath -> FilePath -> IO ()
upperCasesInFile inputFile outputFile = readFile inputFile
                                        >>= (\str ->
                                            let newNames = map (\names -> firstToUpper names) (lines str)
                                            in writeFile outputFile (unlines newNames))

getReverseAndMore' :: IO ()
getReverseAndMore' = do
        putStrLn "Please type a word!"
        str <- getLine
        putStr "Reversed word:"
        putStrLn (reverse str)
        putStr "Head:"
        putChar (head str)
        putChar '\n'
        putStr "Last:"
        putChar (last str)
        putStr "\n"

getMultiplication' :: IO ()
getMultiplication' = do
    putStrLn "Please provide a number:"
    num1 <- getInt
    --putChar '\n'
    putStrLn "Please provide another number:"
    num2 <- getInt
    putStr "Multiplication:"
    print(num1 * num2)