{--
Implement a game where a user needs to guess a secret number. An exemplary round looks as
follows.
*Main> main
Your guess, please!
hello?
Enter a number, please.
13
Too small
Your guess, please!
73
Too large
Your guess, please!
42
Congrats, your guess is correct!
You needed 3 tries to win the game!
The lines “hello?”, “13”, “73” and “42” were entered by the user.
As a first step, define a helper function getInt :: IO Int that handles non-digit input like we see above
for “hello?” until the user enters a positive number. You can use all isDigit :: String -> Bool to
check if a string contains only digits.
Define the function guessNumber that is used in the main function as defined below. Your implementation
of guessNumber should use the helper function getInt and behave as shown in the example.
main :: IO ()
main = do
n <- guessNumber 42
putStrLn ("You needed " ++ show n ++ " tries to win the game!")
--}

{-# LANGUAGE GADTs #-} 
import System.IO 
--import System.Char

main :: IO ()
main = do
    n <- guessNumber 42
    putStrLn ("You needed " ++ show n ++ " tries to win the game!")

guessNumber :: Int -> IO Int
guessNumber n = guess 1
                   where guess try  = do 
                                      putStrLn "Your guess, please!"
                                      val <- getInt
                                      if val == n then do
                                          putStrLn("Congrats, your guess is correct!")
                                          return try
                                      else if val < n then do
                                          putStrLn("Too small")
                                          r <- guess (try + 1) 
                                          return r
                                      else do  
                                          putStrLn("Too large")
                                          r <- guess (try + 1)
                                          return r 

getInt :: IO Int
getInt = do
        str <- getLine
        if isDigit str then
            do
            let num = read str :: Int
            return num
        else 
            do
            putStrLn ("Enter a number")
            r <- getInt
            return r
            
isCharNum :: Char -> Bool
isCharNum '0' = True
isCharNum '1' = True
isCharNum '2' = True
isCharNum '3' = True
isCharNum '4' = True
isCharNum '5' = True
isCharNum '6' = True
isCharNum '7' = True
isCharNum '8' = True
isCharNum '9' = True
isCharNum _   = False

isDigit :: String -> Bool
isDigit []  = False
isDigit str = foldr (\chr accBool -> isCharNum chr && accBool) True str