{-# LANGUAGE GADTs #-} 
import System.IO 
import System.Random

main :: IO()
main = do
        contents <- readFile "words.txt"
        randomNumber <- randomIO :: IO Int
        let words = lines contents 
            n = randomNumber `mod` (length words)
            word = words !! n 
        play word (map (\x -> '*') word) (length word)

play :: String -> String -> Int -> IO()
play word known guesses 
    | word == known = do
                      putStrLn ("Secret:" ++ known)
                      putStrLn ("You won! Solved in " ++ show((length word) - guesses) ++ " tries")
    | guesses == 0  = do
                      putStrLn ("Secret:" ++ known)
                      putStrLn ("You lose. The word was " ++ show(word) ++ ".")
    | otherwise     = do
                      putStrLn ("Secret:" ++ known)
                      putStrLn ("You have " ++ show (guesses) ++ " guesses left!")
                      line <- getLine
                      let (newKnown, newGuesses) = handle (head line) word known guesses
                      play word newKnown newGuesses

handle :: Char -> String -> String -> Int -> (String, Int)
handle letter word known guesses
    | letter `elem` word = (zipWith (\w k -> if w == letter then w else k) word known, guesses)
    | otherwise          = (known, guesses - 1)


