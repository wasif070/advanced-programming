{-# LANGUAGE GADTs #-} 

data Rose a where
    Rose :: a -> [Rose a] -> Rose a

instance (Eq a) => Eq (Rose a) where
    Rose a [] == Rose b [] = a==b
    Rose a as == Rose b bs = a==b && as==bs
    _ == _                 = False

{--
instance (Ord a) => Ord (Rose a) where
    Rose a [] <= Rose b bs = True
    Rose a [] <= Rose b [] = a <= b
    Rose a as <= Rose b [] = False
    Rose a as <= Rose b bs = a <= b || as <= bs
    --}

instance (Ord a) => Ord (Rose a) where
    compare (Rose a []) (Rose b []) = compare a b 
    compare (Rose a []) (Rose b bs) = LT
    compare (Rose a as) (Rose b []) = GT 
    compare (Rose a as) (Rose b bs) = case compare a b of
                                            EQ -> compare as bs
                                            LT -> LT
                                            GT -> GT


{--
Define a type class Pretty with a function pretty :: a -> String to output values 
of any data type as a pretty string. Then specify an instance of the Pretty type class 
for rose trees that indents the tree as follows.

ghci> putStrLn (pretty (Rose 4 [Rose 5 [Rose 1 [], Rose 2 [], Rose 3 []], Rose 6 []]))
4
+-- 5
|   +-- 1
|   +-- 2
|   +-- 3
+-- 6
--}

class Pretty a where
    pretty :: a -> String 

roseVar :: Rose Int
roseVar = Rose 4 [Rose 5 [Rose 1 [], Rose 2 [], Rose 3 []], Rose 6 []] 
    
instance (Show a) => Pretty (Rose a) where
    pretty input = helperFunc input 0

generatePrefix :: Int -> String
generatePrefix level | level == 0 = ""
                     | level == 1 = "+-- "
                     | otherwise  = "|   " ++  [' ' | x <- [1 .. (level -2) * 4]]  ++ "+-- "

helperFunc :: (Show a) => Rose a -> Int -> String
helperFunc (Rose x xs) level = (generatePrefix level) ++ (show x) ++ "\n" ++ (callForAll xs (level+1))

callForAll :: (Show a) => [Rose a] -> Int -> String
callForAll [] _   = "" 
callForAll (y:ys) level = (helperFunc y level) ++ (callForAll ys level)