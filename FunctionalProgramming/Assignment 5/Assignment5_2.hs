{-# LANGUAGE GADTs #-} 
allCombinations :: [a] -> [[a]]

allCombinations xs = allCombinations' [[]]
                    where 
                      allCombinations' lists = let newList = addListToAll xs lists 
                                    in newList ++ (allCombinations' newList)
                     

addListToAll :: [a] -> [[a]] -> [[a]]
addListToAll [] _ = []
addListToAll (x:xs) ys = (addValueToAll x ys) ++ (addListToAll xs ys)

addValueToAll :: a -> [[a]] -> [[a]]
addValueToAll x [] = []
addValueToAll x (y:ys) = (y ++ [x]) : (addValueToAll x ys)