{-# LANGUAGE GADTs #-} 

data SearchTree a where
    Empty :: SearchTree a
    Node  :: a -> SearchTree a -> SearchTree a -> SearchTree a
    deriving (Show)

tree1 :: SearchTree Int
tree1 = Node 15 
            (
                Node 10 
                (
                    Node 8
                       Empty
                       Empty
                ) 
                (
                    Node 14
                       Empty
                       Empty
                )
            ) 
            (
                Node 25 
                (
                    Node 20
                       Empty
                       Empty
                ) 
                (
                    Node 30
                       Empty
                       Empty
                )
            )

--foldr :: (a->b->b) -> b -> [a] -> b

foldNodeLabeledTree :: b -> (a -> b -> b -> b) -> SearchTree a -> b
foldNodeLabeledTree base fBranch  Empty = base 
foldNodeLabeledTree base fBranch (Node x tl tr) = fBranch x (foldNodeLabeledTree base fBranch tl) (foldNodeLabeledTree base fBranch tr)

sum' :: SearchTree Int -> Int
sum' Empty = 0
sum' (Node x tl tr) = x + sum' tl + sum' tr 

sum'' :: SearchTree Int -> Int
sum'' t = foldNodeLabeledTree 0 (\n x y -> n + x + y) t

height :: SearchTree Int -> Int
height Empty = 1
height (Node x tl tr) = 1 + max (height tl) (height tr)

height' :: SearchTree Int -> Int
height' t = foldNodeLabeledTree 1 (\b x y -> 1 + max x y) t

mirror :: SearchTree a -> SearchTree a
mirror Empty = Empty
mirror (Node x tl tr) = Node x (mirror tr) (mirror tl)

mirror' :: SearchTree a -> SearchTree a
mirror' t = foldNodeLabeledTree Empty (\n x y -> Node n y x) t

insert :: Int -> SearchTree Int -> SearchTree Int
insert n Empty                                   = Node n Empty Empty
insert n (Node x leftTree rightTree) | n == x    = Node x leftTree (insert n rightTree)
                                     | n > x     = Node x leftTree (insert n rightTree)
                                     | otherwise = Node x (insert n leftTree) rightTree

--fold :: b -> (a ->b ->b ->b)-> SearchTree a -> b
--insert' :: Int -> SearchTree Int -> SearchTree Int 
--insert' n t = foldNodeLabeledTree (Node n Empty Empty) (\n tl tr -> if n==x then Node n tl tr else undefined) t

isElem :: Int -> SearchTree Int -> Bool
isElem n Empty                                   = False
isElem n (Node x leftTree rightTree) | n == x    = True
                                     | n > x     = isElem n rightTree
                                     | otherwise = isElem n leftTree

deleteElem :: Int -> SearchTree Int -> SearchTree Int
deleteElem n Empty          = Empty
deleteElem n (Node x tl tr) | n == x = if tl == Empty then tr
                                       else if tr == Empty then tl
                                       else let y = getMinVal tr
                                       in Node y tl (deleteElem y tr)
                            | n < x     = Node x (deleteElem n tl) tr
                            | otherwise = Node x tl (deleteElem n tr)


getMinVal :: SearchTree Int -> Int
getMinVal Empty          = 2^30
getMinVal (Node x tl tr) = min x (getMinVal tl)