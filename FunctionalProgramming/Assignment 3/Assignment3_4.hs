{-# LANGUAGE GADTs #-} 

--foldr (:) []
func1 :: [a] -> [a]
func1 []     = []
func1 (x:xs) = x : func1 xs

--foldl (*) 1
func2 :: [Int] -> Int
func2 xs = func2' xs 1 
    where
        func2' :: [Int] -> Int -> Int
        func2' [] carry     = carry
        func2' (x:xs) carry = func2' xs (x*carry)  

--foldr (-) 1
func3 :: [Int] -> Int
func3 [] = 1
func3 (x:xs) = x - (func3 xs)

--foldl (-) 1
func4 :: [Int] -> Int
func4 xs = func4' xs 1
        where
            func4' :: [Int] -> Int -> Int
            func4' [] v     = v
            func4' (x:xs) v = func4' xs (v-x)

--define map and reverse using fold
myMap :: (a -> b) -> [a] -> [b]
myMap f [] = []
myMap f xs = [f x | x <- xs]

myMap' :: (a -> b) -> [a] -> [b]
myMap' f = foldr(\x xs -> (f x) : xs)  []

myReverse :: [a] -> [a]
myReverse xs = myReverse' [] xs
            where myReverse' :: [a] -> [a] -> [a]
                  myReverse' ys []     = ys 
                  myReverse' ys (x:xs) = myReverse' (x:ys) xs

myReverse'' :: [a] -> [a]
--myReverse'' = foldl (flip (:)) []
myReverse'' xs = foldl(\acc x -> x: acc) [] xs
--myReverse'' xs = foldr(\x acc -> acc++[x]) [] xs --reverse using foldr is slow

--foldr is used when we find pure recursive pattern
--foldl is used when we need some accumulator function to solve the problem
--foldi foldt

--myUnzip [(1,2),(2,3),(3,4)] = ([1,2,3],[2,3,4])
myUnzip :: [(a,b)] -> ([a],[b])
myUnzip ((x,y) : [])  = ([x],[y])
myUnzip ((x,y) : xys) = ( x: fst (myUnzip xys), y: snd(myUnzip xys))

--unzip using foldr :: (a -> b->b)-> b -> [a] -> b
myUnzip' :: [(a,b)] -> ([a],[b])
myUnzip' xys = foldr(\(x,y) xys -> (x: fst xys , y : snd xys)) ([],[]) xys

--nub using foldr -- removes duplicates
myNub :: (Eq a) => [a] -> [a]
myNub ys = foldr(\y ys -> y: (filter (/=y) ys)) [] ys

myNub' :: (Eq a) => [a] -> [a]
myNub' [] = []
myNub' (x:xs) | x `elem` xs = myNub' xs
              | otherwise   = x : myNub' xs