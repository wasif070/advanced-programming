{-# LANGUAGE GADTs #-} 

indexOf :: (Eq a) => a -> [a] -> Maybe Int
indexOf n (x:xs) = indexOf' n (x:xs) 0
    where indexOf' :: (Eq a) => a -> [a] -> Int -> Maybe Int
          indexOf' n [] index                 = Nothing
          indexOf' n (x:xs) index | n == x    = Just index
                                  | otherwise = indexOf' n xs (index + 1)
--[1,2] = [[],[1],[1,2]]
inits :: [a] -> [[a]]
inits xs = inits' xs []
    where inits' :: [a] -> [a] -> [[a]]
          inits' [] zs     = [zs]
          inits' (y:ys) zs = zs : inits' ys (zs ++ [y])  

--[1,2] = [[1, 2], [2], []]
tails :: [a] -> [[a]]
tails []     = [[]]
tails (x:xs) = (x:xs) : tails xs 

--insert 1 [2, 3] = [[1, 2, 3], [2, 1, 3], [2, 3, 1]]
insert :: a -> [a] -> [[a]]
insert n xs = insert' n xs 0
                  where
                     insert' :: a -> [a] -> Int -> [[a]]
                     insert' y xs idx | idx > length xs = []   
                                      | otherwise       = [(fst (splitAt idx xs)) ++ [y] ++ (snd (splitAt idx xs))] ++ (insert' y xs (idx+1))

-- The function perms :: [a] -> [[a]] should calculate all permutations of a list.
perms :: (Eq a) => [a] -> [[a]]
perms xs = makeUnique(perms' xs [] 0)

perms' :: [a] -> [a] -> Int -> [[a]]
perms' [] ys idx = [ys]
--perms' xs [] idx = [xs]
perms' xs ys idx | idx + 1 == length xs = perms' (removeValueByIndex idx xs) ((getValueByIndex idx xs) :ys) 0
                 | otherwise            = (perms' (removeValueByIndex idx xs) ((getValueByIndex idx xs) :ys) 0) 
                                                ++ (perms' xs ys (idx+1))

getValueByIndex :: Int -> [a] -> a
getValueByIndex idx []     = error "empty list"
getValueByIndex idx (x:xs) | idx == 0    = x
                           | otherwise = getValueByIndex (idx-1) xs

removeValueByIndex :: Int -> [a] -> [a]
removeValueByIndex idx []     = []
removeValueByIndex idx (x:xs) | idx == 0 = xs
                              | otherwise = x : removeValueByIndex (idx-1) xs

makeUnique :: (Eq a) => [[a]] -> [[a]]
makeUnique [] = []
makeUnique (x:xs) = x : (filter(/=x) (makeUnique xs))
