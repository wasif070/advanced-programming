{-# LANGUAGE GADTs #-} 

data Tree a where
    Leaf :: a -> Tree a
    Node :: Tree a -> Tree a -> Tree a
 deriving Show

treeS :: Tree Int
treeS = Node 
        (
            Node           
                (Leaf 2)
                (Leaf 1)            
        )
        (Leaf 7)

treeR :: Tree Int
treeR = Leaf 6

treeM :: Tree Int
treeM = Node (Leaf 5) (Leaf 6)

complexTree :: Tree (Tree Int)
complexTree = Node 
             ( 
                Node  
                   (Leaf treeS) 
                   (Leaf treeR) 
             ) 
             (Leaf treeM)

flatTree :: Tree (Tree a) -> Tree a
flatTree (Leaf x) = x
flatTree (Node leftTree rightTree) = Node (flatTree leftTree) (flatTree rightTree) 

mapTree  :: (a -> b) -> Tree a -> Tree b
mapTree f (Leaf x) = Leaf (f x)
mapTree f (Node leftTree rightTree) = Node (mapTree f leftTree) (mapTree f rightTree)

foldTree :: (a -> b) -> (b -> b -> b) -> Tree a -> b
foldTree fLeaf fBranch (Leaf x) = fLeaf x
foldTree fLeaf fBranch (Node tl tr) = fBranch (foldTree fLeaf fBranch tl) (foldTree fLeaf fBranch tr)

--mapTree using foldTree
mapTree' :: (a->b) -> Tree a -> Tree b
mapTree' f t = foldTree (\x -> Leaf (f x)) (\tl tr -> Node tl tr) t



---node labeled
{--
foldTree' :: b -> (a -> b -> b -> b) -> Tree a -> b
foldTree' base fBranch Empty = base
foldTree' base fBranch (Node x tl tr) = fBranch x (foldTree' base fBranch tl) (foldTree' base fBranch tr)
-}