{-# LANGUAGE GADTs #-} 

data IntList where
    NoInt      :: IntList
    OneMoreInt :: Int -> IntList -> IntList
 deriving Show