--testFunc :: Int -> Int -> Int 
--testFunc x y = x * y
{-# LANGUAGE GADTs #-} 
import Data.Char
import System.IO 

double x = x + x
quadruple x = double (double x)

isDigit  :: Char -> Bool
isDigit c = c >= '0' && c <= '9'

even1    :: Integral a => a -> Bool
even1 n = n `mod` 2 == 0

splitAt1 :: Int -> [a] -> ([a], [a])
splitAt1 n xs = (take n xs, drop n xs) 

{--
abs :: Int -> Int
abs n = if n >= 0 then n else -n
--}

{--
signum :: Int -> Int
signum n = if n<0 then -1 else
            if n==0 then 0 else 1
--}

abs'   :: Int -> Int
abs' n | n >= 0    = n
       | otherwise = -n

signum1 :: Int -> Int
signum1 n | n<0       = -1
          | n==0      = 0
          | otherwise = 1

fst :: (a, b) -> a
fst (a, _) = a

snd :: (a, b) -> b
snd (_, b) = b

pred        :: Int -> Int
pred 0       = 0
--pred (n + 1) = n

odds :: Int -> [Int]
odds n = map f [0..n-1]
            where f x = x*2 +1            
--odds n = map (λx -> x*2 +1)[0..n-1]

factors :: Int -> [Int]
factors n = [x | x <- [1..n], n `mod`x == 0]

prime :: Int -> Bool
prime n = factors n == [1,n]

primes :: Int -> [Int]
primes n = [x | x <- [2..n], prime x]

lowers :: String -> Int
lowers xs = length[x | x <- xs, isLower x] 

count' :: Char -> String -> Int
count' x xs = length[x' | x' <- xs, x'==x]

let2int              :: Char -> Int
let2int c            = ord c - ord 'a'

int2let              :: Int -> Char
int2let n            = chr(ord 'a' + n)
{--
shif                 :: Int -> Char -> Char
shif n c | isLower c = int2let((let2int c + n) 'mod' 26)
         | otherwise = c
         --}
length' :: [a] -> Int
length' []     = 0
length' (_:xs) = 1 + length' xs

length''       :: [a] -> Int
length'' = foldr (\_ n -> 1 + n) 0

reverse' :: [a] -> [a]
reverse' []       = []
reverse' (x : xs) = reverse xs ++ [x]

insert :: Ord a => a -> [a] -> [a]
insert x []                = [x]
insert x (y:ys) | x<=y     = x:y:ys
                |otherwise = y: insert x ys

isort :: Ord a => [a] -> [a]
isort [] = []
isort (x : xs) = insert x (isort xs)

fibonacci :: Int -> Int
fibonacci 0       = 0
fibonacci 1       = 1
fibonacci n = fibonacci (n-2) + fibonacci (n -1)

qsort :: Ord a => [a] -> [a]
qsort []       = []
qsort (x : xs) = qsort smaller ++ [x] ++ larger
                 where smaller = [a | a <- xs, a<=x]
                       larger  = [b | b <- xs, b >x]

drop' :: Int -> [a] -> [a]
drop' 0 xs     = xs
drop' n []     = []
drop' n (_:xs) = drop (n-1) xs

map'      :: (a -> b) -> [a] -> [b] 
map' f xs = [f x |x <- xs]

filter' :: (a -> Bool) -> [a] -> [a]
filter' p xs = [x | x <- xs, p x]

{--
sum          :: Int -> [Int] -> Int
sum v (x:xs) = sum' 0
                where 
                    sum' v [] = v
                    sum' v (x:xs) = sum' (v+x) xs
                    --}
type Parser a = String -> [(a, String)]
return'        :: a -> Parser a
return' v      = \inp -> [(v,inp)]

data A where
    A :: A
    B :: A -> [A] -> A
    deriving Show

removeNonUpperCase :: [Char] -> [Char]
removeNonUpperCase st = [c | c <- st, c `elem` ['A'..'Z']]


maximum' :: Ord a => [a] -> a
maximum' []     = error "empty list"
maximum' [x]    = x
maximum' (x:xs) | x > maxTail = x
                | otherwise   = maxTail
                where maxTail = maximum' xs

replicate' :: (Num i, Ord i) => i -> a -> [a]
replicate' n x | n <= 0    = []
               | otherwise = x : replicate' (n-1) x 

take' :: (Num i, Ord i) => i -> [a] -> [a]
take' n _        | n <= 0 = []
take' _ []       = []
take' n (x : xs) = x : take' (n-1) xs 

reverse'' :: [a] -> [a]
reverse'' []       = []
reverse'' (x : xs) =  reverse'' xs ++ [x]

zip' :: [a] -> [b] -> [(a,b)]
zip' [] _ = []
zip' _ [] = []
zip' (x : xs) (y : ys) = (x,y) : zip' xs ys


elem' :: (Eq a) => a -> [a] -> Bool
elem' _ []       = False
elem' n (x : xs) | n == x    = True
                 | otherwise = n `elem'` xs


--quicksort = let all the smaller sorted before the head, bigger sorted than head after the head

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x : xs) = let smallerSorted = quicksort [a | a <- xs, a <= x]
                         biggerSorted  = quicksort [a | a <-xs, a > x]
                      in smallerSorted ++ [x] ++ biggerSorted

quicksort' :: (Ord a) => [a] -> [a]
quicksort' [] = []
quicksort' (x : xs) = let smallerSorted1 = quicksort' (filter (<=x) xs)
                          biggerSorted1  = quicksort' (filter (>x) xs)
                        in smallerSorted1 ++ [x] ++ biggerSorted1


zipWith' :: (a -> b-> c) -> [a] -> [b] -> [c]
zipWith' _ [] _          = []
zipWith' _ _ []          = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys   
{--
filter'' :: (a -> Bool) -> [a] -> [a]
filter'' _ []                 = []
filter'' p (x:xs) | p x       = x : filter'' p xs
                 | otherwise = filter''  xs
                 --}

--largest number under 100000 thats divisible by 3829
largestDivisible :: (Integral a) => a
largestDivisible =  head (filter p [100000, 99999 ..])
                    where p x = x `mod` 3829 == 0

--find the sum of all odd squares that are smaller than 10000
sumOfAllOddSquares :: (Integral a) => a
sumOfAllOddSquares = sum (takeWhile (<10000) (filter odd (map (^2) [1..])))


data Point = Point Float Float 
                deriving Show
data Shape = Circle Point Float | Rectangle Point Point
                deriving Show

surface :: Shape -> Float
surface (Circle _ r) = pi * r^2
surface (Rectangle (Point x1 y1) (Point x2 y2)) = (abs (x2-x1)) * (abs (y2-y1))

data Car = Car {
                company :: String,
                model :: String,
                year :: Int
                } deriving Show

zip1 :: [a] -> [b] -> [(a,b)]
zip1 [] _          = []
zip1 _ []          = []
zip1 (x:xs) (y:ys) = (x,y): zip1 xs ys 

unzip1 :: [(a,b)] -> ([a],[b])
unzip1 []           = ([],[])
unzip1 ((x,y): xys) = let (xs, ys) = unzip1 xys
                        in (x:xs, y:ys)


data Tree a = Empty 
              | Node1 a (Tree a)
              | Node2 a (Tree a) (Tree a)
              deriving Show

val = [ x - y | x <- [1,2,3,4,5], y <- [0,0,0,0,0]]



--main = do 
   -- readFile "words.txt" >>= writeFile "words1.txt"
   --readFile "words.txt" >>= print.length. filter(all (== ' ')). lines
   --lines <- readFile "words.txt"
   --do putStrLn (enumrateLines lines)

--enumrateLines :: String -> String
--enumrateLines lines = zip (map(\n -> show n ++ ": ") [1..]) . lines


shiftChar :: Int -> Char -> Char
shiftChar n c | isLower c = chr (((ord c - ord 'a' + n) `mod` 26) + ord 'a')
              | isUpper c = chr (((ord c - ord 'A' + n) `mod` 26) + ord 'A')
              | otherwise = c

shiftString :: String -> Int -> String
shiftString xs n = map (shiftChar n) xs

concat' :: [[a]] -> [a]
concat' [] = []
concat' ([]:xss) = concat' xss
concat' ((x:xs):xss) = x : concat' (xs:xss)

main :: IO ()
main = do
    putStr "n: "
    str <- getLine
    n <- fac (read str)
    putStrLn ("final: " ++ show (n))
    

fac :: Int -> IO Int
fac n = if n==0 then return 1
        else 
            do 
             f <- fac (n-1)
             print (n-1, f)
             return (n*f)