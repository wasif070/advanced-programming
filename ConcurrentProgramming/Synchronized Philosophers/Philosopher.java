public class Philosopher implements Runnable {

    /** The stick to the left of the philosopher */
    private Stick left;

    /** The stick to the right of the philosopher */
    private Stick right;

    /** The number of the philosopher */
    private int num;

    /**
     * Constructor for a philosopher
     *
     * @param num   Number of the philosopher
     * @param left  The left stick
     * @param right The right stick
     */
    public Philosopher(int num, Stick left, Stick right) {
        // your code here
        this.num = num;
        this.left = left;
        this.right = right;
    }

    /**
     * The activity loop of a philosopher
     */
    @Override
    public void run() {

        try {
            while (true) {
                think();
                getStick(left);
                if (lookForStick(right)) {
                    getStick(right);
                    eat();
                    putStick(left);
                    putStick(right);
                }else{
                    putStick(left);
                }
            }
        } catch (Exception e) {
        }

    }

    /**
     * Put the stick back on the table
     *
     * @param stick Stick to be laid back
     */
    public void putStick(Stick stick) {
        stick.put();
        System.out.println(getName() + " puts " + stick.getName());
    }

    /**
     * Eat spaghetti!
     *
     * @throws InterruptedException
     */
    private void eat() throws InterruptedException {
        System.out.println(getName() + " is eating");
        Thread.sleep(1000);
    }

    /**
     * Take the stick!
     *
     * @param stick Stick to be taken
     */
    public void getStick(Stick stick) throws InterruptedException {
        stick.get();
        System.out.println(getName() + " picks " + stick.getName());
    }

    /**
     * Think a lot
     *
     * @throws InterruptedException
     */
    private void think() throws InterruptedException {
        System.out.println(getName() + " is thinking");
        Thread.sleep(1000);
    }

    public boolean lookForStick(Stick stick) {
        return stick.lookFor();
    }

    /**
     * The name of the philosopher
     *
     * @return the name
     */
    public String getName() {
        return "Philosopher#" + num;
    }

    public static void main(String[] args) {

        int num = 5;

        if (args.length > 0) {
            num = Integer.parseInt(args[0]);
        }

        Stick[] sticks = new Stick[num];
        Philosopher[] phils = new Philosopher[num];

        for (int i = 0; i < num; ++i) {
            // add Sticks to array
            sticks[i] = new Stick(i + 1);
        }

        for (int i = 0; i < num; ++i) {
            // add Philosophers to array
            // start Thread
            phils[i] = new Philosopher(i + 1, sticks[i], sticks[(i + 1) % sticks.length]);

            Thread t = new Thread(phils[i]);
            t.start();
        }
    }

}
