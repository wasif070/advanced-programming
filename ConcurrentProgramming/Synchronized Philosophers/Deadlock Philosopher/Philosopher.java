public class Philosopher implements Runnable {

    private Stick leftStick;
    private Stick rightStick;

    public Philosopher(Stick leftStick, Stick rightStick) {
        this.leftStick = leftStick;
        this.rightStick = rightStick;
    }

    public void think() {
        System.out.println(Thread.currentThread().getName() + " is thinking");
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
        }
    }

    public void takeLeftStick() {
        this.leftStick.take();
        System.out.println(Thread.currentThread().getName() + " take left stick");
    }

    public void takeRightStick() {
        this.rightStick.take();
        System.out.println(Thread.currentThread().getName() + " take right stick");
    }

    public void eat() {
        System.out.println(Thread.currentThread().getName() + " is eating");
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
        }

    }

    public void putLeftStick() {
        this.leftStick.put();
        System.out.println(Thread.currentThread().getName() + " put left stick");
    }

    public void putRightStick() {
        this.rightStick.put();
        System.out.println(Thread.currentThread().getName() + " put right stick");
    }

    @Override
    public void run() {
        while (true) {
            think();
            takeLeftStick();
            takeRightStick();
            eat();
            putLeftStick();
            putRightStick();
        }

    }

    public static void main(String[] args) {
        Philosopher[] philosophers = new Philosopher[5];
        Stick[] sticks = new Stick[philosophers.length];

        for (int i = 0; i < sticks.length; i++) {
            sticks[i] = new Stick();
        }

        for (int i = 0; i < sticks.length; i++) {
            Stick leftStick = sticks[i];
            Stick rightStick = sticks[(i + 1) % sticks.length];

            philosophers[i] = new Philosopher(leftStick, rightStick);

            Thread t = new Thread(philosophers[i], "Philosopher-" + (i + 1));
            t.start();
        }
    }
}

class Stick {
    private boolean isUsed = false;

    public synchronized void take() {
        while (this.isUsed) {
            try {
                Thread.sleep(500);
            } catch (Exception e) {
            }

        }
        this.isUsed = true;
    }

    public synchronized void put() {
        this.isUsed = false;
    }

}