public class Account {
    private int balance;
    public static Object obj = new Object();

    public Account(int amount) {
        balance = amount;
    }

    public synchronized int getBalance() {
        return balance;
    }

    public synchronized void deposit(int amount) {
        balance += amount;
    }

    public synchronized void transfer(Account acc, int amount) {
        synchronized (obj) {
            deposit(-amount);
            acc.deposit(amount);
        }
    }

    public synchronized void debit(Account acc, int amount) {
        acc.transfer(this, amount);
    }

    public synchronized boolean safeTransfer(Account acc, int amount) {
        if (balance >= amount) {
            transfer(acc, amount);
            return true;
        } else {
            return false;
        }
    }

    public synchronized boolean safeDebit(Account acc, int amount) {
        if (balance >= amount) {
            debit(acc, amount);
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        Account acc1 = new Account(100);
        Account acc2 = new Account(150);

        Thread t1 = new Thread(new Runnable() {

            @Override
            public void run() {
                acc1.transfer(acc2, 10);
            }
        });

        Thread t2 = new Thread(new Runnable() {

            @Override
            public void run() {
                while (acc2.balance > 0) {
                    acc2.transfer(acc1, 25);
                }
            }
        });

        t1.start();
        t2.start();

        System.out.println("acc1 balance-->" + acc1.getBalance());
        System.out.println("acc2 balance--->" + acc2.getBalance());
    }
}