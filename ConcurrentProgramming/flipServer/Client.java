import java.rmi.*;
import java.net.*;

public class Client {

  public static void main(String[] a) {
    try  {
      String host = a.length >= 1 ? a[0] : "localhost";
      String uri = "rmi://"+host+"/FlipServer";

      FlipServer s = (FlipServer)Naming.lookup(uri);
      s.flip();
      System.out.println("State: "+s.getState());
      s = getOther();
      System.out.println("State: "+s.getState());
    } catch (MalformedURLException|NotBoundException|RemoteException e) {
                System.out.println(e);}
  }
}
