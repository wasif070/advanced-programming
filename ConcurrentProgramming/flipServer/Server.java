import java.rmi.*;
import java.net.*;

public class Server {

  public static void main (String[] args) {
     try {
       String host = args.length >= 1 ? args[0] : "localhost";
       String uri = "rmi://" + host + "/FlipServer";

       FlipServerImpl s = new FlipServerImpl(true);
       Naming.rebind(uri,s);
     } catch (MalformedURLException|RemoteException e) {
       System.out.println("Something went wrong: " + e);
     }
  }
}
