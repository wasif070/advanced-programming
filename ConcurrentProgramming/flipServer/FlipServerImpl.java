import java.rmi.server.*;
import java.rmi.*;

public class FlipServerImpl extends UnicastRemoteObject
                            implements FlipServer {

  private boolean state;
  private FlipServer otherFlip;

  public FlipServerImpl(boolean anotherOne) throws RemoteException {
     state = false;
     if (anotherOne) {
       otherFlip = new FlipServerImpl(false);
     } else {
       otherFlip = null;
     }
  }

  public void flip() {
    state = !state;
  }

  public boolean getState() {
    return state;
  }

  public FlipServer getOther() {
    return otherFlip;
  }
}
