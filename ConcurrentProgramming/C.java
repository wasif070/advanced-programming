public class C {
    private int state = 0;

    public synchronized void printNewState() throws InterruptedException {
        wait();
        System.out.println(state);
    }

    public synchronized void setValue(int v) {
        state = v;
        notify();
        System.out.println("value set");
    }

    public static void main(String[] args) {

        C c = new C();

        Runnable run1 = new Runnable() {
        
            @Override
            public void run() {
                try {
                    c.printNewState();
                } catch (Exception e) {
                    //TODO: handle exception
                }
               
            }
        };
        
        Runnable run2 = new Runnable(){
        
            @Override
            public void run() {
                c.setValue(42);
            }
        };
        
        Thread t1 = new Thread(run1);
        Thread t2 = new Thread(run2);

        t1.start();
        t2.start();
    }
}