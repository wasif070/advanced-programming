import java.util.concurrent.Semaphore;

public class SemaphoreDemo {

    public static void main(String[] args) {
        Semaphore s = new Semaphore(1);

        SharedCounter sharedCounter = new SharedCounter(s);

        Thread t1 = new Thread(sharedCounter, "Thread-1");
        Thread t2 = new Thread(sharedCounter, "Thread-2");
        Thread t3 = new Thread(sharedCounter, "Thread-3");

        t1.start();
        t2.start();
        t3.start();
    }

}

class SharedCounter implements Runnable {
    int c = 0;
    Semaphore s = null;

    public SharedCounter(Semaphore s) {
        this.s = s;
    }

    public void increment() {
        c++;
    }

    public void decrement() {
        c--;
    }

    public int getValue() {
        return c;
    }

    @Override
    public void run() {
        try {
            this.s.acquire();

            this.increment();
            System.out.println(
                    "Value after increment of Thread " + Thread.currentThread().getName() + "=" + this.getValue());

            this.decrement();
            System.out.println(
                    "Value after decrement of Thread " + Thread.currentThread().getName() + "=" + this.getValue());

            this.s.release();

        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}