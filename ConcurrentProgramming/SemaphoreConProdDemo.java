import java.util.concurrent.Semaphore;

public class SemaphoreConProdDemo {

    public static void main(String[] args) {

        Shared shared = new Shared();

        Thread t1 = new Thread(new Producer(shared), "Producer");
        Thread t2 = new Thread(new Consumer(shared), "Consumer");

        t1.start();
        t2.start();
    }
}

class Shared {
    int i = 0;

    Semaphore scon = new Semaphore(0);
    Semaphore sprod = new Semaphore(1);

    public void produce(int i) {
        try {
            sprod.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.i = i;
        System.out.println("Produce-->" + i);
        scon.release();
    }

    public void consume() {
        try {
            scon.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Consume-->" + i);
        sprod.release();
    }
}

class Producer implements Runnable {

    Shared s = null;

    Producer(Shared s) {
        this.s = s;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            s.produce(i);
        }
    }
}

class Consumer implements Runnable {
    Shared s = null;

    Consumer(Shared s) {
        this.s = s;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            s.consume();
        }
    }
}