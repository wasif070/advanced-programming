import java.util.ArrayList;
import java.util.List;

class BufferN<T> {

    List<T> contents;
    int maxSize;

    public BufferN(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        } else {
            maxSize = n;
            contents = new ArrayList<T>();
        }
    }

    public synchronized boolean isEmpty() {
        return contents.isEmpty();
    }

    public synchronized T take() {
        if (!contents.isEmpty()) {
            T content = contents.get(0);
            contents.remove(0);
            return content;
        } else {
            throw new NullPointerException("Buffer empty");
        }
    }

    public synchronized void put(T elem) {
        if (contents.size() < maxSize) {
            contents.add(elem);
        }
    }

    public static void main(String[] args) {

        BufferN buff = new BufferN(3);
        buff.put(4);
        buff.put(3);
        buff.put(2);
        buff.put(1);
        System.out.println(buff.take());
        System.out.println(buff.isEmpty());
        System.out.println(buff.take());
        System.out.println(buff.isEmpty());
        System.out.println(buff.take());
        System.out.println(buff.isEmpty());


    }
}