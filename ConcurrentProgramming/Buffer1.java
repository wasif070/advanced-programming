class Buffer1<T> {

    T content;
    boolean empty;

    Object r = new Object();
    Object w = new Object();

    public Buffer1() {
        content = null;
        empty = true;
    }

    public T take() throws InterruptedException {
        synchronized (r) {
            while (empty) {
                r.wait();
            }
            synchronized (w) {
                w.notify();
                empty = true;
                return content;
            }
        }
    }

    public void put(T value) throws InterruptedException {
        synchronized (w) {
            while (!empty) {
                w.wait();
            }
            synchronized (r) {
                r.notify();
                empty = false;
                content = value;
            }
        }
    }
    /*
     * public boolean tryPut(T content) should put content in the buffer and return
     * true if the buffer is empty. If not, the method should not suspend, unlike
     * put, but return false.
     */

    public boolean tryPut(T content) {
        synchronized (w) {
            if (empty) {
                synchronized (r) {
                    empty = false;
                    this.content = content;
                    return true;
                }
            } else {
                return false;
            }

        }
    }

    /*
     * public T read() should read the buffer contents without emptying the buffer.
     * If the buffer is empty, the method suspends.
     */
    public T read() {
        synchronized (r) {
            if (empty) {
                throw new NullPointerException("Buffer empty");
            } else {
                synchronized (w) {
                    return content;
                }
            }
        }
    }
    /*
     * public void overwrite(T content) should overwrite the buffer content with
     * content without suspending regardless of whether the buffer is full or not.
     */

    public void overwrite(T content) {
        synchronized (w) {
            synchronized (r) {
                this.content = content;
                this.empty = false;
            }
        }
    }

    /*
     * For the take method, specify a variant public T take(long timeout) that only
     * suspends a specified time (in ms). If the time timeout has expired without a
     * value being read, a java.util.concurrent.TimeoutException should be thrown.
     * Be careful not to wait excessively long!
     */
    public T take(long timeout) throws InterruptedException {
        long start_time = System.currentTimeMillis();

        synchronized (r) {
            while (empty) {
                if (System.currentTimeMillis() > (start_time + timeout)) {
                    throw new java.util.concurrent.TimeoutException();
                }
                r.wait();
            }
            synchronized (w) {
                w.notify();
                empty = true;
                return content;
            }
        }
    }
}
