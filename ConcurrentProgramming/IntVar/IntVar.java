
public class IntVar
{
  // TODO: Your code here.
    private int value;

    public IntVar(){
      this.value = 0;
    }
  /**
   * Little helper method to sleep a period of time.
   *
   * @param time The length of time to sleep in milliseconds.
   */
  private static void sleep(long time)
  {
    try {
      Thread.sleep(time);
    } catch (InterruptedException ex) {
      ex.printStackTrace();
    }
  }

  public int getValue()
  {
    // TODO: Your code here.
    return value;
  }

  public void setValue(int value)
  {
    // TODO: Your code here.
    this.value = value;
  }

  public void doubleIt()
  {
    // TODO: Your code here.
    int val = getValue();
    sleep(1000);
    setValue(val * 2);
    
  }

  public void syncDoubleIt()
  {
    // TODO: Your code here.
   synchronized(this){
     setValue(getValue() * 2);
   }

  }

  public void incIt()
  {
    // TODO: Your code here.
    int val = getValue();
    sleep(1000);
    setValue(val+ 1);
  }

  public void syncIncIt()
  {
    // TODO: Your code here.
    synchronized(this){
      setValue(getValue() + 1);
    }
  }

  /**
   * Executes two {@link Runnable}s concurrently, whereas the second one is
   * started after the given delay.
   *
   * @param v     The {@link IntVar} both {@link Runnable}s operate on.
   * @param pause The time to sleep before starting the second
   *              {@link Runnable}.
   * @param r1    The first {@link Runnable} to be executed.
   * @param r2    The second {@link Runnable} to be executed.
   */
  private static void test(final IntVar v, long pause, Runnable r1,
                           Runnable r2)
  {
    Thread t1 = new Thread(r1);
    Thread t2 = new Thread(r2);

    t1.start();
    sleep(pause);
    t2.start();

    try {
      t1.join();
      t2.join();
    } catch (InterruptedException ex) {
      ex.printStackTrace();
    }

    System.out.println(v.getValue());
  }

  // This method yields 0.
  private static void zero()
  {
    final IntVar v = new IntVar();

    // TODO: You might want to adjust this value.
    long sleepingTime = 500;


    Runnable r1 = new Runnable() {
      public void run() 
      {
        // TODO: Your code here.
        v.incIt();
      }
    };

    Runnable r2 = new Runnable() {
      public void run()
      {
        // TODO: Your code here.
        v.doubleIt();
      }
    };

    test(v, sleepingTime, r1, r2);
  }

  // This method yields 1.
  private static void one()
  {
    final IntVar v = new IntVar();

    // TODO: You might want to adjust this value.
    long sleepingTime = 0;

    Runnable r1 = new Runnable() {
      public void run()
      {
        // TODO: Your code here.
        v.doubleIt();
        
      }
    };

    Runnable r2 = new Runnable() {
      public void run()
      {
        // TODO: Your code here.
        v.incIt();

      }
    };

    test(v, sleepingTime, r1, r2);
  }

  // This method yields 2.
  private static void two()
  {
    final IntVar v = new IntVar();

    // TODO: You might want to adjust this value.
    long sleepingTime = 1000;

    Runnable r1 = new Runnable() {
      public void run()
      {
        // TODO: Your code here.
        v.incIt();
      }
    };

    Runnable r2 = new Runnable() {
      public void run()
      {
        // TODO: Your code here.
        v.doubleIt();
      }
    };

    test(v, sleepingTime, r1, r2);
  }

  // This method yields 2.
  private static void syncZero()
  {
    final IntVar v = new IntVar();

    // TODO: You might want to adjust this value.
    long sleepingTime = 0;

    Runnable r1 = new Runnable() {
      public void run()
      {
        // TODO: Your code here.
        v.syncIncIt();
      }
    };

    Runnable r2 = new Runnable() {
      public void run()
      {
        // TODO: Your code here.
        v.syncDoubleIt();
      }
    };

    test(v, sleepingTime, r1, r2);
  }

  public static void main(String[] args)
  {
    zero();
    one();
    two();
    syncZero();
  }
}