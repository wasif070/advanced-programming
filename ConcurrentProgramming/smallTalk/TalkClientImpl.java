import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

/** This implements a simple chat client via RMI. */
public class TalkClientImpl extends UnicastRemoteObject implements TalkClient
{
  /** The other client. */
  private TalkClient other;

  /** Creates a new client with the given connection peer. */
  public TalkClientImpl(TalkClient other) throws RemoteException
  {
    this.other = other;
  }

  /** Create a new registry or connect to an existing one. */
  private static Registry getOrCreateRegistry()
  {
    try {
      return LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
    } catch (RemoteException ex) {
      try {
        return LocateRegistry.getRegistry();
      } catch (RemoteException rex) {
        return null;
      }
    }
  }

  /** Receives a call. */
  public synchronized void connect(TalkClient other) throws RemoteException
  {
    this.other = other;
    notify();
  }

  /** Receives a goodbye message. */
  public synchronized void bye() throws RemoteException
  {
    other = null;
    System.out.println("*** Partner left the talk.");
  }

  /** Receives a message from the server and prints it to the output. */
  public void send(String msg) throws RemoteException
  {
    System.out.println(msg);
  }

  /** Establishes a connection. */
  private synchronized void waitForOther() throws RemoteException
  {
    if (other == null) {
      System.out.println("*** Waiting for the talk partner.");
      try {
        wait();
      } catch (InterruptedException ex) {
        ex.printStackTrace();
      }
    } else {
      other.connect(this);
    }
  }

  /** A simple read-print-loop which reads messages from the input. */
  public void talk() throws RemoteException
  {
    boolean connected = false;

    waitForOther();
    connected = true;

    System.out.println("Welcome to talk, type ':q' to quit.");

    try (Scanner in = new Scanner(System.in)) {
      String msg;

      while (connected) {
        msg = in.nextLine();

        synchronized (this) {
          if (other == null) {
            ;
          } else if (msg.equals(":q")) {
            other.bye();
            connected = false;
          } else {
            other.send(msg);
          }
        }
      }
    }
  }

  /** Starts a new talk client. */
  public static void main(String[] args) throws Exception
  {
    Registry reg;
    TalkClientImpl tcImpl;

    switch (args.length) {
      case 1:
        reg = getOrCreateRegistry();
        tcImpl = new TalkClientImpl(null);
        reg.bind(args[0], tcImpl);
        tcImpl.talk();
        reg.unbind(args[0]);
        break;
      case 2:
        reg = LocateRegistry.getRegistry(args[1]);
        TalkClient tc = (TalkClient) reg.lookup(args[0]);
        tcImpl = new TalkClientImpl(tc);
        tcImpl.talk();
        break;
      default:
        System.err.println("Usage:");
        System.err.println("To receive a call: java TalkClientImpl <name>");
        System.err.println("To call someone  : java TalkClientImpl <name> <host>");
        System.exit(1);
      }

      System.exit(0);
  }
}