import java.rmi.Remote;
import java.rmi.RemoteException;

/** This is an interface for a simple RMI talk client. */
public interface TalkClient extends Remote
{
  /** Connects to the talk client. */
  public void connect(TalkClient other) throws RemoteException;

  /** Hangs up a connection. */
  public void bye() throws RemoteException;

  /** Sends a message to the talk client. */
  public void send(String msg) throws RemoteException;
}