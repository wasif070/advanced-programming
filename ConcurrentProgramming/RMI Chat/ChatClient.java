import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChatClient extends Remote
{
  /** Receives a message from the server and prints it. */
  public void send(String msg) throws RemoteException;
}