import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ChatServer extends Remote
{
  public final String RMI_NAME = "ChatServer";

  /** Registers the given client at the server. */
  public boolean register(ChatClient c, String name) throws RemoteException;

  /** Returns the collection of users currently logged in. */
  public List<String> getUsers() throws RemoteException;

  /** Removes the given client from the server. */
  public void logout(ChatClient c) throws RemoteException;

  /** Sends the given message to all connected clients. */
  public void send(String msg) throws RemoteException;
}